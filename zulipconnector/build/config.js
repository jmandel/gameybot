"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.generateMeta = exports.port = void 0;
exports.port = process.env.PORT || 8080;
var SERVER_META = {
    "url": process.env.BASE_URL || "http://localhost:" + exports.port
};
exports.generateMeta = function () { return (__assign(__assign({}, SERVER_META), { ts: new Date().getTime() })); };
