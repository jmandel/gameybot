"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.makeRouter = void 0;
var express_1 = __importDefault(require("express"));
var config_1 = require("./config");
function makeRouter(commands, reduce) {
    var router = express_1.default.Router();
    router.get("/commands.json", function (req, res) {
        res.json({
            meta: config_1.generateMeta(),
            commands: commands
        });
    });
    router.post("/event", function (req, res) {
        var _a = req.body, incomingState = _a.state, incomingMessage = _a.message;
        var _b = reduce(incomingState, incomingMessage), nextState = _b.state, nextMessages = _b.messages;
        res.json({
            meta: config_1.generateMeta(),
            state: nextState,
            messages: nextMessages
        });
    });
    return router;
}
exports.makeRouter = makeRouter;
