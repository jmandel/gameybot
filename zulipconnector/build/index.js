"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.app = void 0;
var body_parser_1 = __importDefault(require("body-parser"));
var express_1 = __importDefault(require("express"));
var config_1 = require("./config");
var fs_1 = __importDefault(require("fs"));
exports.app = express_1.default();
exports.app.use(body_parser_1.default.json());
fs_1.default.
    readdirSync(__dirname).
    filter(function (f) { return f.match(/^game\..*\.[jt]s/u); }).
    forEach(function (f) {
    var game = require("./" + f).default;
    console.log("Gane", f, game);
    exports.app.use("/api/" + game.name, game.router);
    console.log("Registered game", game.name);
});
/*
 *Games.forEach((game) => {
 *    app.use(`/api/${game.name}`, game.router);
 *});
 */
var server = exports.app.listen(config_1.port, function () {
    console.log("server started on port " + config_1.port + ". Public URL " + config_1.generateMeta().url);
});
exports.default = server;
