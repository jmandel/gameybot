"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __spreadArrays = (this && this.__spreadArrays) || function () {
    for (var s = 0, i = 0, il = arguments.length; i < il; i++) s += arguments[i].length;
    for (var r = Array(s), k = 0, i = 0; i < il; i++)
        for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++)
            r[k] = a[j];
    return r;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var base64_url_1 = __importDefault(require("base64-url"));
var canvas_1 = require("canvas");
var rfdc_1 = __importDefault(require("rfdc"));
var shuffle_array_1 = __importDefault(require("shuffle-array"));
var config_1 = require("./config");
var words_json_1 = __importDefault(require("./words.json"));
var game_routes_1 = require("./game-routes");
var name = "codenames";
var deepCopy = rfdc_1.default();
var commands = [
    {
        command: "spymaster",
        description: "claim the role of spymaster on the red or blue team. Example: `%spymaster red`"
    },
    {
        command: "clue",
        description: "provide a clue for your team. Example: `%clue watershed 3`"
    },
    {
        command: "done",
        description: "indicate that you're done guessing for this turn -- if you're stopping early. Example: `%done`"
    },
    {
        command: "guess",
        description: "submit a guess (or several) for your team. Example: `%guess crane bark hand`"
    }
];
var boardImage = function (board) { return config_1.generateMeta().url + "/api/codenames/draw/" + base64_url_1.default.encode(JSON.stringify(board)); };
var revealedBoardImage = function (board) { return boardImage(board.map(function (w) { return (__assign(__assign({}, w), { revealed: true })); })); };
var otherTeam = function (t) { return t === "red" ? "blue" : "red"; };
var initialState = function () {
    // Start a new game
    var firstPlayer = Math.random() < 0.5 ? "red" : "blue";
    var shuffledWords = shuffle_array_1.default(__spreadArrays(words_json_1.default)).slice(0, 25);
    var shuffledColors = shuffle_array_1.default(__spreadArrays([
        firstPlayer,
        "black"
    ], Array(8).fill("red"), Array(8).fill("blue"), Array(7).fill("tan")));
    var nextState = {
        finished: false,
        cluingTeam: firstPlayer,
        guesses: [],
        board: Array(25).fill(0).
            map(function (_, i) { return ({
            word: shuffledWords[i],
            color: shuffledColors[i],
            revealed: false
        }); }),
        history: []
    };
    return {
        state: nextState,
        messages: [
            {
                public: true,
                text: "Welcome to Codenames! " + firstPlayer + " team goes first.\n" +
                    "Type `%spymaster red` or `%spymaster blue` to claim the spymaster role.",
                image: boardImage(nextState.board)
            }
        ]
    };
};
function reduce(state, message) {
    if (!state) {
        return initialState();
    }
    if ((message === null || message === void 0 ? void 0 : message.command) === "spymaster") {
        return reduceSpymaster(state, message);
    }
    if ((message === null || message === void 0 ? void 0 : message.command) === "clue") {
        return reduceClue(state, message);
    }
    if ((message === null || message === void 0 ? void 0 : message.command) === "guess" || (message === null || message === void 0 ? void 0 : message.command) === "done") {
        return reduceGuess(state, message);
    }
    return {
        state: state,
        messages: [message]
    };
}
function reduceSpymaster(state, message) {
    var _a;
    var forTeam = message.text.trim().split(/\s+/u)[1];
    var spymasterProperty = forTeam === "red" ? "spymasterRed" : "spymasterBlue";
    return {
        state: __assign(__assign({}, state), (_a = { history: __spreadArrays(state.history, [
                    message
                ]) }, _a[spymasterProperty] = message.player, _a)),
        messages: [
            {
                public: true,
                text: message.player + " is the spymaster for " + forTeam + "."
            },
            {
                public: false,
                player: message.player,
                text: "As the spymaster, you get access to the hidden board state!",
                image: revealedBoardImage(state.board)
            }
        ]
    };
}
function reduceClue(state, message) {
    if (!state.spymasterBlue || !state.spymasterRed) {
        return {
            state: state,
            messages: [
                {
                    public: true,
                    text: "Please tell me who the `%spymaster` is before giving a clue!"
                }
            ]
        };
    }
    var _a = message === null || message === void 0 ? void 0 : message.text.trim().split(/\s+/u).slice(1), word = _a[0], count = _a[1];
    return {
        state: __assign(__assign({}, state), { clue: {
                word: word,
                count: parseInt(count, 10)
            } }),
        messages: []
    };
}
function reduceGuess(state, message) {
    var board = deepCopy(state.board);
    var countedGuessesThisRound = deepCopy(state.guesses);
    var results = [];
    var endOfTurn = (message === null || message === void 0 ? void 0 : message.command) === "done";
    var guessedBlack = false;
    var guesses = message.text.toUpperCase().trim().
        split(/\s+/u).
        slice(1);
    var processGuesses = function (acc, nextGuess) {
        var unmatched = __spreadArrays(acc.unmatched, [
            nextGuess
        ]);
        var toMatch = unmatched.join(" ");
        var card = board.find(function (c) { return c.word === toMatch; });
        if (card) {
            return __assign(__assign({}, acc), { unmatched: [], cards: __spreadArrays(acc.cards, [
                    card
                ]) });
        }
        return __assign(__assign({}, acc), { unmatched: unmatched });
    };
    var validGuesses = guesses.reduce(processGuesses, {
        unmatched: [],
        cards: []
    });
    console.log("Reviewed Guesses", guesses, validGuesses);
    if (validGuesses.unmatched.length > 0) {
        return {
            state: state,
            messages: [
                {
                    public: true,
                    text: "That guess included an unrecognized word: " + validGuesses.unmatched[0] + ". Please try again."
                }
            ]
        };
    }
    for (var _i = 0, _a = validGuesses.cards; _i < _a.length; _i++) {
        var guessedCard = _a[_i];
        guessedCard.revealed = true;
        countedGuessesThisRound.push(guessedCard.word);
        results.push("\"*" + guessedCard.word + "*\" was **" + guessedCard.color + "**.");
        var guessedWrongColor = guessedCard.color !== state.cluingTeam;
        var guessedMaxWords = countedGuessesThisRound.length === state.clue.count + 1;
        if (guessedWrongColor || guessedMaxWords) {
            endOfTurn = true;
            if (guessedCard.color === "black") {
                guessedBlack = true;
            }
            break;
        }
    }
    var endOfGame = false;
    var winner = null;
    var remaining = board.filter(function (w) { return !w.revealed && w.color === state.cluingTeam; }).length;
    if (remaining === 0) {
        endOfGame = true;
        winner = state.cluingTeam;
    }
    else if (guessedBlack) {
        endOfGame = true;
        winner = otherTeam(state.cluingTeam);
    }
    var publicMessage = "" + results.join("\n");
    if (endOfGame) {
        return {
            state: __assign(__assign({}, state), { finished: true, board: board, history: __spreadArrays(state.history, [
                    message
                ]), guesses: countedGuessesThisRound }),
            messages: [
                {
                    public: true,
                    text: publicMessage + "\nAnd that's the game! Congratulations " + winner + " team!",
                    image: boardImage(board)
                }
            ]
        };
    }
    if (!endOfTurn) {
        return {
            state: __assign(__assign({}, state), { board: board, clue: undefined, history: __spreadArrays(state.history, [
                    message
                ]), guesses: countedGuessesThisRound }),
            messages: [
                {
                    public: true,
                    text: publicMessage + "\nKeep guessing or say `%done` if you want to hand it back to " + otherTeam(state.cluingTeam) + ".",
                    image: boardImage(board)
                }
            ]
        };
    }
    var nextState = {
        guesses: [],
        finished: false,
        history: __spreadArrays(state.history, [
            message
        ]),
        board: board,
        cluingTeam: otherTeam(state.cluingTeam)
    };
    return {
        state: nextState,
        messages: [
            {
                public: true,
                text: publicMessage + "\nNext up, " + nextState.cluingTeam + "!",
                image: boardImage(board)
            }
        ]
    };
}
var router = game_routes_1.makeRouter(commands, reduce);
router.get("/draw/:spec", function (req, res) {
    var spec = JSON.parse(base64_url_1.default.decode(req.params.spec));
    var colWidth = 150;
    var rowHeight = 80;
    var pad = 5;
    var canvas = canvas_1.createCanvas(colWidth * 5 + pad * 6, rowHeight * 5 + pad * 6);
    var ctx = canvas.getContext("2d");
    ctx.fillStyle = "white";
    ctx.fillRect(0, 0, canvas.width, canvas.height);
    ctx.textAlign = "center";
    ctx.font = "bold 20px Droid Sans";
    spec.forEach(function (w, i) {
        var row = Math.floor(i / 5);
        var col = i % 5;
        if (w.revealed) {
            ctx.fillStyle = w.color;
        }
        else {
            ctx.fillStyle = "white";
        }
        ctx.fillRect(pad + (colWidth + pad) * col, pad + (rowHeight + pad) * row, colWidth, rowHeight);
        if (w.revealed) {
            ctx.fillStyle = "white";
        }
        else {
            ctx.fillStyle = "black";
        }
        ctx.fillText(w.word, (col + 1) * pad + colWidth * (col + 0.5), (row + 1) * pad + rowHeight * (row + 0.6));
    });
    canvas.createPNGStream().pipe(res);
});
exports.default = {
    name: name,
    router: router
};
