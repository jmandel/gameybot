"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __spreadArrays = (this && this.__spreadArrays) || function () {
    for (var s = 0, i = 0, il = arguments.length; i < il; i++) s += arguments[i].length;
    for (var r = Array(s), k = 0, i = 0; i < il; i++)
        for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++)
            r[k] = a[j];
    return r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var game_routes_1 = require("./game-routes");
var name = "pingpong";
var commands = [
    {
        command: "ping",
        description: "Send a ping to the bot. Example: `%ping howdy`"
    },
    {
        command: "pong",
        description: "Send a pong to the bot (?). Example: `%pong howdy`"
    }
];
function reduce(state, message) {
    var history = __spreadArrays((state === null || state === void 0 ? void 0 : state.history) || [], message ? [message] : []);
    if (!state || !message) { // Start a new game
        return {
            state: {
                turns: 0,
                finished: false,
                history: history
            },
            messages: [
                {
                    public: true,
                    text: "Welcome to pingpong, a game that never ends. You go first!"
                }
            ]
        };
    }
    var response = message.command === "ping" ? "pong" : "ping";
    var q = message.text.split(/\s+/u, 1).
        slice(1).
        join(" ");
    return {
        state: __assign(__assign({}, state), { turns: state.turns + 1, history: history }),
        messages: [
            {
                public: true,
                text: message.player + " here's my " + response + " back: **" + q + "**. Great turn #" + (state.turns + 1) + "!"
            }
        ]
    };
}
var router = game_routes_1.makeRouter(commands, reduce);
exports.default = {
    name: name,
    router: router
};
