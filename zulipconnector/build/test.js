"use strict";
/* eslint no-unused-expressions: "off"*/
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var index_1 = __importDefault(require("./index"));
var chai_http_1 = __importDefault(require("chai-http"));
var chai_1 = __importStar(require("chai"));
require("mocha");
chai_1.default.use(chai_http_1.default);
describe("End to end", function () {
    it("Should produce commands", function () { return __awaiter(void 0, void 0, void 0, function () {
        var commands;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4 /*yield*/, chai_1.default.request(index_1.default).get("/api/codenames/commands.json")];
                case 1:
                    commands = _a.sent();
                    chai_1.expect(commands.status).to.equal(200);
                    chai_1.expect(commands.body.commands).to.have.length.greaterThan(3);
                    return [2 /*return*/];
            }
        });
    }); });
    it("Should play a game", function () { return __awaiter(void 0, void 0, void 0, function () {
        var initializedResponse, initialized, spymaster1, spymaster2, rightAnswers, neutralAnswers, clue1, guess, clueForEight, crazyGuess, poorGuess, invaludGuess;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4 /*yield*/, chai_1.default.request(index_1.default).post("/api/codenames/event")];
                case 1:
                    initializedResponse = _a.sent();
                    chai_1.expect(initializedResponse.status).to.equal(200);
                    initialized = initializedResponse.body;
                    chai_1.expect(initialized.state).to.have.property("board");
                    return [4 /*yield*/, chai_1.default.request(index_1.default).post("/api/codenames/event").
                            send({
                            state: initialized.state,
                            message: {
                                command: "spymaster",
                                player: "player-spymaster-red",
                                text: "%spymaster red"
                            }
                        })];
                case 2:
                    spymaster1 = (_a.sent()).body;
                    return [4 /*yield*/, chai_1.default.request(index_1.default).post("/api/codenames/event").
                            send({
                            state: spymaster1.state,
                            message: {
                                command: "spymaster",
                                player: "player-spymaster-blue",
                                text: "%spymaster blue"
                            }
                        })];
                case 3:
                    spymaster2 = (_a.sent()).body;
                    chai_1.expect(spymaster2.state.spymasterRed).to.equal("player-spymaster-red");
                    chai_1.expect(spymaster2.state.spymasterBlue).to.equal("player-spymaster-blue");
                    chai_1.expect(spymaster2.messages[0].public).to.be.true;
                    chai_1.expect(spymaster2.messages[1].public).to.be.false;
                    rightAnswers = spymaster2.state.board.
                        filter(function (w) { return w.color === spymaster2.state.cluingTeam; }).
                        map(function (w) { return w.word; });
                    neutralAnswers = spymaster2.state.board.
                        filter(function (w) { return w.color === "tan"; }).
                        map(function (w) { return w.word; });
                    return [4 /*yield*/, chai_1.default.request(index_1.default).post("/api/codenames/event").
                            send({
                            state: spymaster2.state,
                            message: {
                                command: "clue",
                                player: "player-spymaster-blue",
                                text: "%clue exclue 4"
                            }
                        })];
                case 4:
                    clue1 = (_a.sent()).body;
                    chai_1.expect(clue1.state.clue).to.include({
                        word: "exclue",
                        count: 4
                    });
                    return [4 /*yield*/, chai_1.default.request(index_1.default).post("/api/codenames/event").
                            send({
                            state: clue1.state,
                            message: {
                                command: "guess",
                                player: "player-spymaster-blue",
                                text: "%guess " + rightAnswers.
                                    slice(0, 5).
                                    join(" ")
                            }
                        })];
                case 5:
                    guess = (_a.sent()).body;
                    chai_1.expect(guess.state.board.filter(function (w) { return w.revealed; })).to.have.length(5);
                    chai_1.expect(guess.state.clue).to.be.undefined;
                    return [4 /*yield*/, chai_1.default.request(index_1.default).post("/api/codenames/event").
                            send({
                            state: spymaster2.state,
                            message: {
                                command: "clue",
                                player: "player-spymaster-blue",
                                text: "%clue crazygood 8"
                            }
                        })];
                case 6:
                    clueForEight = (_a.sent()).body;
                    return [4 /*yield*/, chai_1.default.request(index_1.default).post("/api/codenames/event").
                            send({
                            state: clueForEight.state,
                            message: {
                                command: "guess",
                                player: "player-spymaster-blue",
                                text: "%guess " + rightAnswers.
                                    slice(0, 9).
                                    join(" ")
                            }
                        })];
                case 7:
                    crazyGuess = (_a.sent()).body;
                    chai_1.expect(crazyGuess.state.board.filter(function (w) { return w.revealed; })).to.have.length(9);
                    chai_1.expect(crazyGuess.state.finished).to.be.true;
                    return [4 /*yield*/, chai_1.default.request(index_1.default).post("/api/codenames/event").
                            send({
                            state: clueForEight.state,
                            message: {
                                command: "guess",
                                player: "player-spymaster-blue",
                                text: "%guess\n                    \n                       " + rightAnswers.
                                    slice(0, 2).
                                    join(" ") + "\n                        \n                        " + neutralAnswers.
                                    slice(0, 1).
                                    join(" ") + " \n                                           \n                       " + rightAnswers.
                                    slice(2, 9).
                                    join(" ") + "\n                        "
                            }
                        })];
                case 8:
                    poorGuess = (_a.sent()).body;
                    chai_1.expect(poorGuess.state.board.filter(function (w) { return w.revealed; })).to.have.length(3);
                    chai_1.expect(poorGuess.state.finished).to.be.false;
                    return [4 /*yield*/, chai_1.default.request(index_1.default).post("/api/codenames/event").
                            send({
                            state: clueForEight.state,
                            message: {
                                command: "guess",
                                player: "player-spymaster-blue",
                                text: "%guess\n                    \n                       " + rightAnswers.
                                    slice(0, 2).
                                    join(" ") + "\n                \n                BADGUESS\n                "
                            }
                        })];
                case 9:
                    invaludGuess = (_a.sent()).body;
                    chai_1.expect(invaludGuess.state.board.filter(function (w) { return w.revealed; })).to.have.length(0);
                    return [2 /*return*/];
            }
        });
    }); });
});
