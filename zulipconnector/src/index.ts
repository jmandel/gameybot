import bodyParser from "body-parser";
import express from "express";
import * as config from "./config"
import { port, generateMeta } from "./config";
import axios from "axios";
import fs from "fs";
import querystring from "querystring";
import { GameState, GameMessage } from "./GameInterfaces";
import FormData from 'form-data';
import request from 'request';


import { Base64 } from 'js-base64';


interface Db {
    games: {
        [k: string]: {
            gameName: string,
            eventUrl: string,
            state: any,
            playersToIds: {
                [k: string]: string
            }
        }
    }
}
const db: Db = {
    games: {}
}

export const app = express();
app.use(bodyParser.json());

app.post('/api/incoming', async (req, res, next) => {

    const sender = req.body.message.sender_full_name as string;
    const senderId = "" + (req.body.message.sender_id as number);
    const mention = (u: string) => `@**${u}**`
    const mentionSender = mention(sender)
    console.log("Incoming from ", sender)
    /*
    axios.interceptors.request.use(request => {
        console.log('Starting Request', request)
        return request
    })
    */


    const whichGame = (msg: string) => {
        const mentions = msg.match(/play ([^\s]+)/);
        let ret = mentions?.[1]
        if (ret?.endsWith(".")) {
            return ret.slice(0, ret.length - 1)
        }
        return ret;
    }

    const stateKey = req.body.recipient_id as string;
    const publicMessages: string[] = [];

    let nextState: GameState;
    let nextMessages: GameMessage[] = [];

    const game = db.games[stateKey];
    const gameName = whichGame(req.body.data);
    if (gameName) {
        try {
        let eventUrl = `${config.GAME_SERVER}/${gameName}/event`;
        let response = (await axios.post(eventUrl)).data
        nextState = response.state;
        nextMessages = response.messages;
        db.games[stateKey] = {
            gameName,
            state: nextState,
            eventUrl,
            playersToIds: {
                [sender]: senderId
            }
        }
        console.log("New game", gameName, db.games[stateKey])
        } catch (e) {
            console.log("Couldn't start", e)
            next(e)
        }
    } else {
        const message: GameMessage = {
            public: true,
            text: req.body.data.slice("@**Gameybot** ".length),
            command: req.body.data.split(/\s+/)[1].slice(1).trim(),
            player: sender as string
        }
        try {
            const response = (await axios.post(game.eventUrl, {
                state: game.state,
                message
            })).data

            nextState = response.state;
            nextMessages = response.messages;
            db.games[stateKey] = {
                ...db.games[stateKey],
                state: nextState,
                playersToIds: {
                    ...game.playersToIds,
                    [sender]: senderId
                }
            }

        } catch (e) {
            console.log("Failed to submit", e)
        }
    }

    try {
        const prepareMessages = nextMessages.map(async (m, i) => {
            console.log("Prepare", m.public, m.text)
            const postTo = `${config.ZULIP_SITE}/api/v1/messages`;
            let imageUrl = null;
            if (m.image) {
                const formData = new FormData();
                formData.append('game.png', (await axios.get(m.image, {
                    responseType: 'arraybuffer'
                })).data, {
                    filename: 'image.png',
                    contentType: 'image/png'
                });
                try {

                    const posted = await axios.post(`${config.ZULIP_SITE}/api/v1/user_uploads`, formData, {
                        auth: {
                            username: config.ZULIP_OUTGOING_BOT_ID,
                            password: config.ZULIP_OUTGOING_API_KEY
                        },
                        headers: {
                            ...formData.getHeaders()
                        }
                    })
                    imageUrl = posted.data.uri

                } catch (e) {
                    console.log("E", e)
                }
            }

            if (m.public) {
                const messageText = m.text + (m.image ? `\n[image.png](${imageUrl})` : "");
                publicMessages[i] = messageText
            } else {
                sendMessage(game.playersToIds[m.player!], m, imageUrl);
            }
        })

        await Promise.all(prepareMessages);
    } catch (e) {
        console.log("Failed to process outgoing messages", e)
    }
    if (publicMessages.length) {
        res.json({
            content: publicMessages.filter(m => !!m).join("\n")
        })
    } else {
         res.json({
            response_not_required: true
        })

    }
})


const server = app.listen(
    port,
    () => {

        console.log(`server started on port ${port}. Public URL ${generateMeta().url}`);

    }
);

export default server;
``

function sendMessage(senderId: string, m: GameMessage, imageUrl: any) {
    console.log("Send private", senderId, m)
    axios.post(`${config.ZULIP_SITE}/api/v1/messages`, querystring.encode({
        type: "private",
        to: `[${senderId}]`,
        content: m.text + (m.image ? `\n[image.png](${imageUrl})` : "")
    }), {
        auth: {
            username: config.ZULIP_OUTGOING_BOT_ID,
            password: config.ZULIP_OUTGOING_API_KEY
        },
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        }
    });
}
