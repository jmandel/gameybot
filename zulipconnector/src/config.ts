export const port = process.env.PORT || 5002;

export const ZULIP_OUTGOING_API_KEY = process.env.ZULIP_OUTGOING_API_KEY as string;
export const ZULIP_OUTGOING_BOT_ID = process.env.ZULIP_OUTGOING_BOT_ID as string;
export const ZULIP_SITE = process.env.ZULIP_SITE as string;

export const GAME_SERVER = process.env.GAME_SERVER || 'https://games.flushlyft.com/api'


const SERVER_META = {
    "url": process.env.BASE_URL || `http://localhost:${port}`
};

export const generateMeta = () : {url: string, ts: number} => ({
    ...SERVER_META,
    ts: new Date().getTime()
});


