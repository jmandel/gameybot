export interface GameState {
    finished: boolean;
    history: any[];
}
export interface GamePlayer {
    id: string;
    name: string;
}
export interface GameMessage {
    gamePlayer?: GamePlayer;
    command?: string;
    text: string;
    public: boolean;
    image?: string;
}
export interface IncomingEvent {
    gameId: number;
    state?: GameState;
    message?: GameMessage;
}

export interface GameResponse {
    state: GameState,
    messages: GameMessage[]
}

export interface GameCommand {
    command: string,
    description: string
}
