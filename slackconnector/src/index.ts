import express from "express";
import { port, slackToken, slackSigningSecret } from "./config";
import { WebClient, SectionBlock, ImageBlock, WebAPICallResult } from "@slack/web-api";
import { createEventAdapter } from "@slack/events-api";
import axios from "axios";
import bodyParser from "body-parser";
import { GameResponse, GameState, GameMessage } from "./GameInterfaces";

const slackClient = new WebClient(slackToken);
const slackEvents = createEventAdapter(slackSigningSecret);

export const app = express();

interface SendToSlackMessage extends GameMessage {
  topLevelText?: string
}

interface ConversationRepliesMessage {
  bot_id?: string,
  text: string,
  channel: string,
  ts: string
}
interface ConversationHistoryResult extends WebAPICallResult {
  messages: ConversationRepliesMessage[]
}

interface UserProfileGetResult extends WebAPICallResult {
  profile: {
    bot_id?: string,
    display_name?: string,
    first_name: string
  }
}

interface GameDbEntry {
  gameName: string,
  gameUrl: string,
  gameState: GameState
}

const apiBaseDefault = 'https://games.flushlyft.com/api';
const myBotIdPromise = getMyBotId();

slackEvents.on('app_mention', async (event) => {
  console.log('app_mention', event);
  const topLevelMessage = event.ts === event.thread_ts || !event.thread_ts;
  if (!event.bot_id && topLevelMessage && event.text.match(/play/)) {
    // slack adds the '<>' bookending url-looking string; parse out optionally
    const parseOpen = event.text.match(/play ([^\s]+) ?<?([^\s>]*)>?/);
    if (!parseOpen) {
      return
    }

    const whichGame = parseOpen[1];
    const apiBase = parseOpen[2] || apiBaseDefault;

    const availableCommands = (await axios.get(`${apiBase}/${whichGame}/commands.json`)).data

    const gameUrl = `${apiBase}/${whichGame}/event`
    const gameStarted: GameResponse = (await axios.post(gameUrl)).data

    const nextState = gameStarted.state;
    const nextMessages = gameStarted.messages;
    console.log("Needs to send out messages", nextMessages);

    if (nextMessages.length > 3) {
      throw `TOO MANY MESSAGES -- BAD GAME, ${nextMessages.length}`
    }

    const threadToStart: string = event.ts;
    const gameId = new GameId(event.channel, threadToStart);

    const rulesMessage: SendToSlackMessage = {
      text: `You probably need rules. Here: \n\`\`\`\n${
        JSON.stringify(availableCommands.commands, null, 2)}
        \`\`\`
      `,
      public: true,
      topLevelText: new PersistMessage.Write('').text
    };
    await sendMessages([rulesMessage], event.channel, threadToStart);
    
    // note: above rules message needs to have been sent to thread before attempting to save
    // TODO: might go ahead and initialize rules message with save state to begin with
    await saveGameState(gameId, {
      gameName: whichGame,
      gameUrl,
      gameState: nextState
    });

    sendMessages(nextMessages, event.channel, threadToStart);
  }
})

// note: will receive notification of own messages
slackEvents.on('message', async (event) => {
  console.log('message', event);

  if (!event.thread_ts || event.bot_id) {
    return;
  }
  const gameId = new GameId(event.channel, event.thread_ts);
  const known = await(getGameState(gameId));
  if (event.type === 'message' && known) {
    console.log(`I see activity on game ${gameId}`);
    console.log(known);
    const commandText = (event.text as string)
    if (!commandText.startsWith('%')){
      return;
    }

    // TODO: memoize id->name mappings
    // TODO: can probably use users.profile.get and UserProfileGetResult rather than users.info
    const userProfile = (await slackClient.users.profile.get({user: event.user}) as UserProfileGetResult).profile
    const playerName: string = userProfile.display_name || userProfile.first_name;

    const command = commandText.split(/\s+/)[0].slice(1)

    const eventPayload: {
      state: GameState,
      message: GameMessage
    } = {
      state: known.gameState,
      message: {
        public: true,
        command,
        text: event.text,
        gamePlayer: {
          id: event.user,
          name: playerName
        }
      }
    }
    const gameContinued: GameResponse = (await axios.post(known.gameUrl, eventPayload)).data;

    await saveGameState(gameId, {
      ...known,
      gameState: gameContinued.state
    });
    
    console.log("Needs to send out messages", gameContinued.messages);
    sendMessages(gameContinued.messages, event.channel, event.thread_ts);
 
  }

  if (!event.bot_id) {
    console.log(`Received a message event: user ${event.user} in ${event.channel} with channel type ${event.channel_type} says ${event.text}`);
  }
});

slackEvents.on('error', (error) => {
  console.log(`Error ${error.name} processing message`);
});

app.use('/slack/events', slackEvents.requestListener());
app.use(bodyParser.json());

app.post('/blocks', async function (req, res) {
  const channel = req.body.channel || '#botdevelopment';
  const baseSlackMessage = {
    channel: channel,
    text: req.body.text || `The current time is ${new Date().toTimeString()}`,
    blocks: req.body.blocks,
    thread_ts: req.body.thread_ts
  }
  try {
    const user = req.body.user;
    if (user) {
      const privateSlackMessage = {
        ...baseSlackMessage,
        user: user
      };
      await slackClient.chat.postEphemeral(privateSlackMessage);
      console.log(`sending blocks to ${user} in ${channel}`);
    } else {
      await slackClient.chat.postMessage(baseSlackMessage);
      console.log(`sending blocks to ${channel}`);
    }
  } catch (error) {
    console.log(error);
  }
  res.send('sent blocks')
})

if (!port) {
  throw "NEED A PORT!"
}

const server = app.listen(
  port,
  () => {
    console.log(`server started on port ${port}`);
  }
);

export default server;
function sendMessages(nextMessages: SendToSlackMessage[], channel: string, threadId?: string) {
  const promises = nextMessages.map(m => {
    const blocks: (SectionBlock | ImageBlock)[] = [{
      type: "section",
      text: {
        type: "mrkdwn",
        text: m.text
      }
    }];

    if (m.image) {
      blocks.push({
        type: "image",
        image_url: m.image!,
        alt_text: "Alt image text"
      });
    }

    const baseSlackMessage = {
      channel,
      text: m.topLevelText || m.text,
      blocks,
      thread_ts: threadId,
    };
    if (m.public) {
      return slackClient.chat.postMessage(baseSlackMessage);
    } else {
      const privateSlackMessage = {
        ...baseSlackMessage,
        user: m.gamePlayer!.id
      };
      return slackClient.chat.postEphemeral(privateSlackMessage);
    }
  });
  return Promise.all(promises);
}

interface ThreadId {
  readonly channel: string,
  readonly threadTs: string
}
class GameId implements ThreadId {
  constructor(public readonly channel: string, public readonly threadTs: string) {}

  toString(): string {
    return `${this.channel}-${this.threadTs}`;
  }
}

namespace PersistMessage {
  const TextPrefix = 'BOT_STATE.';
  //const TextRegex = new RegExp('BOT_STATE\.(.*)');

  export class Write {
    public readonly text: string;
    constructor(value: string) {
      this.text = TextPrefix + value;
    }
  }

  export class Read {
    public static isValid(m: ConversationRepliesMessage): boolean {
      return m.text.startsWith(TextPrefix);
    }
    
    public readonly text: string;
    public readonly ts: string;
    constructor(m: ConversationRepliesMessage) {
      this.text = m.text.slice(TextPrefix.length);
      this.ts = m.ts;
    }
  }
}

async function getGameState(gameId: GameId) {
  return await(findPersistMessage(gameId).then(result => {
    return JSON.parse(result.text) as GameDbEntry;
  }).catch(rejectedReason => {
    console.log(`failed to retrieve state from thread: ${rejectedReason}`);
    return undefined;
  }));
}

async function saveGameState(gameId: GameId, entry: GameDbEntry) {
  const message = await findPersistMessage(gameId);
  slackClient.chat.update({
    channel: gameId.channel,
    ts: message.ts,
    text: new PersistMessage.Write(JSON.stringify(entry)).text,
    parse: "none"                         // avoid slack rewriting urls/user ids with extra characters
  });
}

async function findPersistMessage(gameId: GameId): Promise<PersistMessage.Read> {
  const myBotId = await myBotIdPromise;
  console.log(`looking for message authored by me: ${myBotId}`);
  const replies = await slackClient.conversations.replies({
    channel: gameId.channel,
    ts: gameId.threadTs,
    oldest: gameId.threadTs,
    limit: 5
  });
  const result = (replies as ConversationHistoryResult).messages.find(
    m => m.bot_id === myBotId && PersistMessage.Read.isValid(m)
  );
  if (!result) {
    throw 'Failed to find appropriate persist message';
  } else {
    return new PersistMessage.Read(result);
  }
}

async function getMyBotId() {
  return ((await slackClient.users.profile.get()) as UserProfileGetResult).profile.bot_id;
}