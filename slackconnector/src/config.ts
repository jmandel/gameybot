export const port = process.env.PORT;

export const slackToken = process.env.SLACK_TOKEN || '';
export const slackSigningSecret = process.env.SLACK_SIGNING_SECRET || '';
