## Register a slack app

  - https://api.slack.com/apps?new_granular_bot_app=1
  - Request the 'chat.write', 'channels.history', 'app_mentions.read', 'users.profile:read' scopes
  - Copy the value of the bot token and signing secret
  - Choose a unique subdomain for local tunneling, TUNNEL_SUBDOMAIN (I use 'mick-slackconnector')

## Establish a local tunnel via flushlyft serveo
In a long running-terminal, or background:
```
  ssh -J root@flushlyft.com -R ${TUNNEL_SUBDOMAIN}:80:localhost:8070 -p 2222 localhost
```
There are other possible tools for this, like ngrok, which provides some nice extra features, like a local debug interface and request replays, but it is harder to get a fixed subdomain.

## Build and run in docker
From project root directory:
```  
  export SLACK_TOKEN=...
  export SLACK_SIGNING_SECRET=...
  docker build -t slackconnector -f Dockerfile.slackconnector .
  docker run --env SLACK_TOKEN --env SLACK_SIGNING_SECRET -p 8070:8070 --rm -it slackconnector
  curl -X POST http://localhost:8070/blocks
  curl -X POST https://${TUNNEL_SUBDOMAIN}.mickrocosm.com/blocks
```

## Enable Events from Slack
  - On https://api.slack.com/apps/<your_local_app_id>/event-subscriptions, turn "Enable Events" on.
  - For Request URL, provide: https://${TUNNEL_SUBDOMAIN}.mickrocosm.com/slack/events.
  - If your app and tunnel are running, it should verify.

### Subscribe to bot Events
  Subscribe to the following event types (will update the scopes your app requests and force a re-install). Long-term, we may only need message.channels, app_mention:
  - message.channels
  - message.groups
  - message.im
  - message.mpim
  - app_mention
