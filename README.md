## Setup slack connector service
(Grab SLACK_TOKEN and SLACK_SIGNING_SECRET from https://api.slack.com/apps/A015F4NKW9M/oauth)
```
ssh dokku@flushlyft.com apps:create slackconnector
ssh dokku@flushlyft.com docker-options:add slackconnector  build '--file Dockerfile.slackconnector'
ssh dokku@flushlyft.com config:set slackconnector SLACK_TOKEN=${SLACK_TOKEN}
ssh dokku@flushlyft.com config:set slackconnector SLACK_SIGNING_SECRET=${SLACK_SIGNING_SECRET}
git remote add dokku-slackconnector dokku@flushlyft.com:slackconnector
git push dokku-slackconnector some-branch:master
ssh dokku@flushlyft.com letsencrypt slackconnector
```

## Setup zulip connector service
(Grab tokens from Zulip settings)
```
ssh dokku@flushlyft.com apps:create zulipconnector
ssh dokku@flushlyft.com docker-options:add zulipconnector  build '--file Dockerfile.zulipconnector'
ssh dokku@flushlyft.com config:set zulipconnector ZULIP_OUTGOING_API_KEY=${ZULIP_OUTGOING_API_KEY}
ssh dokku@flushlyft.com config:set zulipconnector ZULIP_OUTGOING_BOT_ID=${ZULIP_OUTGOING_BOT_ID}
ssh dokku@flushlyft.com config:set zulipconnector ZULIP_SITE=${ZULIP_SITE}
ssh dokku@flushlyft.com config:set zulipconnector GAME_SERVEr=${GAME_SERVER}
git remote add dokku-zulipconnector dokku@flushlyft.com:zulipconnector
git push dokku-zulipconnector
ssh dokku@flushlyft.com letsencrypt zulipconnector
```



## Setup games service
```
ssh dokku@flushlyft.com apps:create games
ssh dokku@flushlyft.com docker-options:add games build '--file Dockerfile.games'
ssh dokku@flushlyft.com config:set games BASE_URL=https://games.flushlyft.com
git remote add dokku-games dokku@flushlyft.com:games
git push dokku-games some-branch:master
ssh dokku@flushlyft.com letsencrypt games
```

## Setup private serveo instance
### Get wildcard *.tunnel.flushlyft.com cert
Follow https://www.digitalocean.com/community/tutorials/how-to-acquire-a-let-s-encrypt-certificate-using-dns-validation-with-acme-dns-certbot-on-ubuntu-18-04, ending with cert files in /etc/letsencrypt/live/tunnel.flushlyft.com/:
```
root@gameybot:~# certbot certonly --manual --manual-auth-hook /etc/letsencrypt/acme-dns-auth.py --preferred-challenges dns --debug-challenges -d \*.tunnel.flushlyft.com -d tunnel.flushlyft.com
...
```

### Configure tunnel app
```
ssh dokku@flushlyft.com apps:create tunnel
ssh dokku@flushlyft.com domains:add tunnel tunnel.flushlyft.com *.tunnel.flushlyft.com
ssh dokku@flushlyft.com config:set tunnel DOKKU_DOCKERFILE_START_CMD="serveo -port=22 -private_key_path=/root/.ssh/id_ed25519 -domain tunnel.flushlyft.com"
ssh dokku@flushlyft.com docker-options:add tunnel deploy "-p 2222:22"
```

### Install tunnel cert
```
root@gameybot:~# mkdir tunnel.flushlyft.com-certs/
root@gameybot:~tunnel.flushlyft.com-certs# cd tunnel.flushlyft.com-certs
root@gameybot:~tunnel.flushlyft.com-certs# cp /etc/letsencrypt/live/tunnel.flushlyft.net/fullchain.pem server.crt
root@gameybot:~/tunnel.flushlyft.com-certs# cp /etc/letsencrypt/live/tunnel.flushlyft.net/privkey.pem server.key
root@gameybot:~/tunnel.flushlyft.com-certs# tar cvf certs.tar server.key server.crt
root@gameybot:~/tunnel.flushlyft.com-certs# dokku certs:add tunnel < certs.tar
```

### Tag/deploy dockerhub image for tunnel app
```
root@gameybot:# docker pull taichunmin/serveo-server
root@gameybot:# docker tag taichunmin/serveo-server dokku/tunnel:taichunmin
root@gameybot:# dokku tags:deploy tunnel taichunmin
```

### Current Issues with tunnel app
- Manual port bind by docker with "-p 2222:22" means dokku can't easily restart. Some config changes require "dokku ps:stop/start" cycle.
- Wildcard cert will be renewed automatically(?) by non-dokku-managed certbot, but cert will still need manually installation into dokku
- Restart of tunnel app will regenerate app's ssh keys (generated in running container), so subsequent connections will not recognize fingerprint. Should store key in volume?
- Uses image from https://github.com/taichunmin/docker-serveo-server, with opaque serveo binary. Could use https://github.com/antoniomika/sish with more explicit/documented options.

## Deploy
```
git push dokku-slackconnector some-branch:master
git push dokku-games some-branch:master
```

## Tail remote logs
```
ssh -t dokku@flushlyft.com logs slackconnector -t
ssh -t dokku@flushlyft.com logs games -t
ssh -t dokku@flushlyft.com logs tunnel -t
```

## Debug message formatting with '/blocks':
```
curl -X POST -H "Content-Type: application/json" --data @slackconnector/examples/blocks-body1.json https://slackconnector.flushlyft.com/blocks
```
