## Build and run in docker
From project root directory:
```sh
docker build -t games -f Dockerfile.games .
docker run -p 8080:8080 --rm -it games
curl http://localhost:8080/api/codenames/commands.json
```

## Developing
From `games` directory:

* `npm test` run tests
* `npm lint-fix` apply automatic lint fixes, when possible
* `npm run dev` run in "watch" mode, restarting when files change
