import express, {Router} from "express";
import {GameState, GameMessage, GameResponse, GameCommand} from "./GameInterfaces";
import {generateMeta} from "./config";

type ReduceGameEvent<
    S extends GameState,
    M extends GameMessage> = (
    state: S,
    message: M) => GameResponse

export function makeRouter<S extends GameState, M extends GameMessage> (
    commands: GameCommand[],
    reduce: ReduceGameEvent<S, M>
): Router {
    const router = express.Router();

    router.get(
        "/commands.json",
        (req, res) => {

            res.json({
                meta: generateMeta(),
                commands
            });

        }
    );

    router.post(
        "/event",
        (req, res) => {

            const {state: incomingState, message: incomingMessage} = req.body;
            const {state: nextState, messages: nextMessages} = reduce(
                incomingState,
                incomingMessage
            );

            res.json({
                meta: generateMeta(),
                state: nextState,
                messages: nextMessages
            });

        }
    );

    return router;
}
