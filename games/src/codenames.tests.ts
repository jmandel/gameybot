/* eslint no-unused-expressions: "off"*/

import server from "./index";
import chaiHttp from "chai-http";
import chai, {expect} from "chai";
import "mocha";

chai.use(chaiHttp);

describe("End to end", () => {
    it("Should produce commands", async () => {
        const commands = await chai.request(server).get("/api/codenames/commands.json");

        expect(commands.status).to.equal(200);
        expect(commands.body.commands).to.have.length.greaterThan(3);
    });
    it("Should play a game", async () => {
        const initializedResponse = await chai.request(server).post("/api/codenames/event");

        expect(initializedResponse.status).to.equal(200);

        const initialized = initializedResponse.body;

        expect(initialized.state).to.have.property("board");

        const spymaster1 = (await chai.request(server).post("/api/codenames/event").
            send({
                state: initialized.state,
                message: {
                    command: "spymaster",
                    gamePlayer: {
                        id: "player-spymaster-red",
                        name: "Spymaster Red"
                    },
                    text: "%spymaster red"
                }
            })).body;

        const spymaster2 = (await chai.request(server).post("/api/codenames/event").
            send({
                state: spymaster1.state,
                message: {
                    command: "spymaster",
                    gamePlayer: {
                        id: "player-spymaster-blue",
                        name: "Spymaster Blue"
                    },
                    text: "%spymaster blue"
                }
            })).body;

        expect(spymaster2.state.spymasterRed).to.deep.equal({
            id: "player-spymaster-red",
            name: "Spymaster Red"
        });
        expect(spymaster2.state.spymasterBlue).to.deep.equal({
            id: "player-spymaster-blue",
            name: "Spymaster Blue"
        });
        expect(spymaster2.messages[0].public).to.be.true;
        expect(spymaster2.messages[1].public).to.be.false;

        const rightAnswers = spymaster2.state.board.
            filter((w: any) => w.color === spymaster2.state.cluingTeam).
            map((w: any) => w.word);

        const neutralAnswers = spymaster2.state.board.
            filter((w: any) => w.color === "tan").
            map((w: any) => w.word);

        const clue1 = (await chai.request(server).post("/api/codenames/event").
            send({
                state: spymaster2.state,
                message: {
                    command: "clue",
                    gamePlayer: {
                        id: "player-spymaster-blue",
                        name: "Spymaster Blue"
                    },
                    text: "%clue exclue 4"
                }
            })).body;

        expect(clue1.state.clue).to.include({
            word: "exclue",
            count: 4
        });

        const guess = (await chai.request(server).post("/api/codenames/event").
            send({
                state: clue1.state,
                message: {
                    command: "guess",
                    gamePlayer: {
                        id: "player-spymaster-blue",
                        name: "Spymaster Blue"
                    },
                    text: `%guess ${rightAnswers.
                        slice(0, 5).
                        join(" ")}`
                }
            })).body;

        expect(guess.state.board.filter((w: any) => w.revealed)).to.have.length(5);
        expect(guess.state.clue).to.be.undefined;

        const clueForEight = (await chai.request(server).post("/api/codenames/event").
            send({
                state: spymaster2.state,
                message: {
                    command: "clue",
                    gamePlayer: {
                        id: "player-spymaster-blue",
                        name: "Spymaster Blue"
                    },
                    text: "%clue crazygood 8"
                }
            })).body;


        const crazyGuess = (await chai.request(server).post("/api/codenames/event").
            send({
                state: clueForEight.state,
                message: {
                    command: "guess",
                    gamePlayer: {
                        id: "player-spymaster-blue",
                        name: "Spymaster Blue"
                    },
                    text: `%guess ${rightAnswers.
                        slice(0, 9).
                        join(" ")}`
                }
            })).body;

        expect(crazyGuess.state.board.filter((w: any) => w.revealed)).to.have.length(9);
        expect(crazyGuess.state.finished).to.be.true;


        const poorGuess = (await chai.request(server).post("/api/codenames/event").
            send({
                state: clueForEight.state,
                message: {
                    command: "guess",
                    gamePlayer: {
                        id: "player-spymaster-blue",
                        name: "Spymaster Blue"
                    },
                    text: `%guess
                    
                       ${rightAnswers.
                slice(0, 2).
                join(" ")}
                        
                        ${neutralAnswers.
                slice(0, 1).
                join(" ")} 
                                           
                       ${rightAnswers.
                slice(2, 9).
                join(" ")}
                        `

                }
            })).body;

        expect(poorGuess.state.board.filter((w: any) => w.revealed)).to.have.length(3);
        expect(poorGuess.state.finished).to.be.false;

        const invaludGuess = (await chai.request(server).post("/api/codenames/event").
            send({
                state: clueForEight.state,
                message: {
                    command: "guess",
                    gamePlayer: {
                        id: "player-spymaster-blue",
                        name: "Spymaster Blue"
                    },
                    text: `%guess
                    
                       ${rightAnswers.
                slice(0, 2).
                join(" ")}
                
                BADGUESS
                `

                }
            })).body;

        expect(invaludGuess.state.board.filter((w: any) => w.revealed)).to.have.length(0);

    });


});


