export const port = process.env.PORT || 8080;

export const SERVER_META = {
    "url": process.env.BASE_URL || `http://localhost:${port}`
};

export const generateMeta = () : {url: string, ts: number} => ({
    ...SERVER_META,
    ts: new Date().getTime()
});


