import bodyParser from "body-parser";
import express from "express";
import {port, generateMeta} from "./config";
import fs from "fs";

export const app = express();
app.use(bodyParser.json());

fs.
    readdirSync(__dirname).
    filter((f) => f.match(/^game\..*\.[jt]s/u)).
    forEach((f) => {
        const game = require(`./${f}`).default;

        app.use(`/api/${game.name}`, game.router);
    });


/*
 *Games.forEach((game) => {
 *    app.use(`/api/${game.name}`, game.router);
 *});
 */

const server = app.listen(
    port,
    () => {

        console.log(`server started on port ${port}. Public URL ${generateMeta().url}`);

    }
);

export default server;
