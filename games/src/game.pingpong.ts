import {GameMessage, GameState, GameCommand} from "./GameInterfaces";
import {makeRouter} from "./game-routes";

const name = "pingpong";


interface PingPongGameMessage extends GameMessage {
    command: "ping" | "pong"
}

interface PingPongGameState extends GameState {
    history: PingPongGameMessage[],
    turns: number
}

const commands: GameCommand[] = [
    {
        command: "ping",
        description: "Send a ping to the bot. Example: `%ping howdy`"
    },
    {
        command: "pong",
        description: "Send a pong to the bot (?). Example: `%pong howdy`"
    }
];

function reduce (state?: PingPongGameState, message?: PingPongGameMessage): {
    state: PingPongGameState,
    messages: GameMessage[]
} {

    const history = [
        ...state?.history || [],
        ...message ? [message] : []
    ];

    if (!state || !message) { // Start a new game
        return {
            state: {
                turns: 0,
                finished: false,
                history

            },
            messages: [
                {
                    public: true,
                    text: "Welcome to pingpong, a game that never ends. You go first!"
                }
            ]
        };
    }

    const response = message.command === "ping" ? "pong" : "ping";
    const q = message.text.split(/\s+/u, 1).
        slice(1).
        join(" ");
    const playerName = message.gamePlayer!.name;

    return {
        state: {
            ...state,
            turns: state.turns + 1,
            history
        },
        messages: [
            {
                public: true,
                text: `${playerName} here's my ${response} back: **${q}**. Great turn #${state.turns + 1}!`
            }
        ]
    };
}

const router = makeRouter<PingPongGameState, PingPongGameMessage>(commands, reduce);

export default {
    name,
    router
};
