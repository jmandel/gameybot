/* eslint no-unused-expressions: "off"*/

import chai, {expect} from "chai";
import chaiHttp from "chai-http";
import "mocha";
import {ScrabbleGameResponse} from "./game.scrabble";
import {GameMessage, GameResponse} from "./GameInterfaces";
import server from "./index";

chai.use(chaiHttp);

const play = async (game: string, moves: string[]): Promise<GameResponse> => {

    const prelude = [
        "josh %join",
        "carrie %join",
        "josh %first"
    ];
    const initializedResponse = (await chai.request(server).post(`/api/${game}/event`)).body;

    const response = initializedResponse as any;

    response.state.bag = "ABALONEROKEREX".split("").concat(response.state.bag);

    return playOn(game, response, prelude.concat(moves));
};

const playOn = async (game: string, lastResponse: ScrabbleGameResponse, moves: string[]): Promise<GameResponse> => {
    const gameCommands = parseCommands(moves);

    let response = lastResponse;

    for (const cmd of gameCommands) {
        response = (await chai.request(server).post(`/api/${game}/event`).
            send({
                state: response.state,
                message: cmd
            })).body as ScrabbleGameResponse;
    }

    return response;
};

function parseCommands (moves: string[]): GameMessage[] {
    return moves.map((m):GameMessage => {
        const arr = m.split(/\s+/u);
        const id = arr[0];
        const name = `My Name Is ${id}`;

        return {
            public: true,
            gamePlayer: {
                id,
                name
            },
            command: arr[1].slice(1),
            text: arr.slice(1).join(" ")
        };
    });
}


describe("End to end", () => {
    it("Should play a simple game", async () => {
        const response = await play("scrabble", [
            "josh %play 8B ABALONE",
            "carrie %play C8 BROKEREX"
        ]) as ScrabbleGameResponse;

        console.log("response", response);
        expect(response.state.players[0].score).to.equal(70);
        expect(response.state.players[1].score).to.equal(94);
    });
    it("Should allow a valid challenge", async () => {
        const responseBeforeChallengedPlay = await play("scrabble", ["josh %play 8B ABALONE"]) as ScrabbleGameResponse;
        const responseAfterChallenge = await playOn("scrabble", responseBeforeChallengedPlay, [
            "carrie %play C8 BROKEREX",
            "josh %challenge BROKEREX"
        ]) as ScrabbleGameResponse;

        const before = responseBeforeChallengedPlay.state;
        const after = responseAfterChallenge.state;

        expect(after.currentPlayerIx).to.equal(0);
        expect(after.players[after.currentPlayerIx].id).to.equal("josh");
        expect(after.players[1].rack).to.deep.equal(before.players[1].rack);
        expect(after.players[1].score).to.equal(before.players[1].score);
        expect(after.players[0].score).to.equal(before.players[0].score);
        expect(after.positions).to.deep.equal(before.positions);
    });
    it("Should fail an invalid challenge", async () => {
        const response = await play("scrabble", [
            "josh %play 8B ABALONE",
            "carrie %play C8 BROKER",
            "josh %challenge BROKER"
        ]) as ScrabbleGameResponse;

        expect(response.state.currentPlayerIx).to.equal(1);
        expect(response.state.players[response.state.currentPlayerIx].id).to.equal("carrie");
        expect(response.state.players[1].rack).to.have.length(7);
    });
    it("Should fail two turns in a row by Josh", async () => {
        const response = await play("scrabble", [
            "josh %play 8B ABALONE",
            "josh %play C8 BROKER"
        ]) as ScrabbleGameResponse;

        expect(response.messages[0].text).to.match(/^Error/u);
    });
    it("Should fail a play going off the board with a message to that effect", async () => {
        const response = await play("scrabble", [
            "josh %play 8H ABALONE",
            "carrie %play 7N BROKER"
        ]) as ScrabbleGameResponse;

        expect(response.messages[0].text).to.match(/^Error.*bound/u);
    });
});


