/*
 * TODO
 * * handle words too long for the board
 */

import {createCanvas, loadImage} from "canvas";
import _ from "lodash";
import querystring from "querystring";
import rfdc from "rfdc";
import shuffleArray from "shuffle-array";
import {generateMeta} from "./config";
import {makeRouter} from "./game-routes";
import {GameCommand, GameMessage, GameResponse, GameState, GamePlayer} from "./GameInterfaces";
import fs from "fs";
import path from "path";


const name = "scrabble";
const deepCopy = rfdc();

const boardSize = 15;

const allWords = fs.
    readFileSync(path.join(__dirname, "Collins-Scrabble-Words-2019.txt")).toString().
    split(/\r\n/u).
    reduce((acc: Record<string, true|undefined>, w) => {
        acc[w] = true;

        return acc;
    }, {});

console.log(Object.keys(allWords).length);

type Point = [number, number]
type Line = [Point, Point]
type Tile = "BLANK" | "A" | "B" | "C" | "D" | "E" | "F" | "G" | "H" | "I" | "J" | "K" | "L" | "M" |
    "N" | "O" | "P" | "Q" | "R" | "S" | "T" | "U" | "V" | "W" | "X" | "Y" | "Z"

type TileDetail = {
    count: number,
    score: number
};

type BoardBonus = "triple-letter" | "double-letter" | "triple-word" | "double-word"

type TileDetails = Record<Tile, TileDetail>;
const pointValues: TileDetails = {
    "A": {
        count: 9,
        score: 1
    },
    "B": {
        count: 2,
        score: 3
    },
    "C": {
        count: 2,
        score: 3
    },
    "D": {
        count: 4,
        score: 2
    },
    "E": {
        count: 1,
        score: 1
    },
    "F": {
        count: 2,
        score: 4
    },
    "G": {
        count: 3,
        score: 2
    },
    "H": {
        count: 2,
        score: 4
    },
    "I": {
        count: 9,
        score: 1
    },
    "J": {
        count: 1,
        score: 8
    },
    "K": {
        count: 1,
        score: 5
    },
    "L": {
        count: 4,
        score: 1
    },
    "M": {
        count: 2,
        score: 3
    },
    "N": {
        count: 6,
        score: 1
    },
    "O": {
        count: 8,
        score: 1
    },
    "P": {
        count: 2,
        score: 3
    },
    "Q": {
        count: 1,
        score: 10
    },
    "R": {
        count: 6,
        score: 1
    },
    "S": {
        count: 4,
        score: 1
    },
    "T": {
        count: 6,
        score: 1
    },
    "U": {
        count: 4,
        score: 1
    },
    "V": {
        count: 2,
        score: 4
    },
    "W": {
        count: 2,
        score: 4
    },
    "X": {
        count: 1,
        score: 8
    },
    "Y": {
        count: 2,
        score: 4
    },
    "Z": {
        count: 1,
        score: 10
    },
    "BLANK": {
        count: 2,
        score: 0
    }
};

type Tuple<TItem, TLength extends number> = [TItem, ...TItem[]] & { length: TLength };
type Tuple15<T> = Tuple<T, 15>;
type Tuple15x15<P> = Tuple<Tuple15<P>, 15>;

type Diff<T, U> = T extends U ? never : T; // Remove types from T that are assignable to U

type BoardPosition = {
    tile?: Tile,
    letter?: Diff<Tile, "BLANK">
    bonus?: BoardBonus
}
type Board = Tuple15x15<BoardPosition>;

export const blankBoard: Board = Array(15).fill(0).
    map(() => Array(15).fill(0).
        map(() => ({} as BoardPosition))) as Board;


const reflectRight = ([
    r,
    c]:
    Point): Point => [
    r,
    boardSize - 1 - c
];
const reflectDown = ([
    r,
    c]:
    Point): Point => [
    boardSize - 1 - r,
    c
];
const reflectDiagnoal = ([
    r,
    c]:
    Point): Point => [
    c,
    r
];

const reflectAll = (point: Point): Point[] => [point].
    flatMap((p) => [
        p,
        reflectRight(p)
    ]).
    flatMap((p) => [
        p,
        reflectDown(p)
    ]).
    flatMap((p) => [
        p,
        reflectDiagnoal(p)
    ]);

const bonusToPositions: Record<BoardBonus, Point[]> = {
    "triple-letter": [
        [
            1,
            5
        ],
        [
            5,
            5
        ]
    ],
    "triple-word": [
        [
            0,
            0
        ],
        [
            0,
            7
        ]
    ],
    "double-letter": [
        [
            0,
            3
        ],
        [
            2,
            6
        ],
        [
            3,
            7
        ],
        [
            6,
            6
        ]
    ],
    "double-word": [
        [
            1,
            1
        ],
        [
            2,
            2
        ],
        [
            3,
            3
        ],
        [
            4,
            4
        ],
        [
            7,
            7
        ]
    ]
};

Object.entries(bonusToPositions).forEach(([
    bonus,
    positions
]) => {
    positions.
        flatMap(reflectAll).
        forEach(([
            r,
            c
        ]) => {
            blankBoard[r][c].bonus = bonus as BoardBonus;
        });
});


const fullBag: Tile[] = Object.entries(pointValues).flatMap(([
    k,
    v
]) => Array(v.count).fill(k));

interface TilePlayed {
    tile: Tile,
    letter: Diff<Tile, "BLANK">,
    position: [number, number]
}
export type Turn = {
    player: GamePlayer,
    turnType: string
} & ({
    turnType: "played-tiles"
    played: TilePlayed[],
} | {
    turnType: "traded-tiles",
    count: number
} | {
    turnType: "issued-challenge",
    objectingTo: string
});

type ScrabbleCommand = "join" | "first" | "play" | "challenge" | "trade"

interface ScrabbleGameMessage extends GameMessage {
    command: ScrabbleCommand
}

export interface ScrabbleGameState extends GameState {
    bag: Tile[],
    currentPlayerIx: number,
    players: {
        id: string,
        name: string,
        score: number,
        rack: Tile[],
        words: ScoredWord[][]
    }[],
    positions: Board,
    previousTurn?: ScrabbleGameState
}

export interface ScrabbleGameResponse extends GameResponse {
    state: ScrabbleGameState,
    messages: GameMessage[]
}

const commands: GameCommand[] = [
    {
        command: "play",
        description: `Play a word onto the board. Example: \`%play 3A PONY\` to play the tiles *P*, *O*, *N*, *Y*
        going across starting at 3A, or \`%play A3 PONY\` to play the same tiles going down. If you want to play a
        blank, you should denote this with a lower-case letter -- e.g., \`%play 3a POnY\` to use your blank for the
        *N*. Note that you'll list all tiles in your word, even if some are already on the board.`
    },
    {
        command: "challenge",
        description: "Challenge the previous player's word. Example: `%challenge buhloneey`"
    },
    {
        command: "trade",
        description: "Trade in some of your tiles for new tiles from the bag. Example: `%trade 3` to trade 3 tiles"
    }
];

const reduceInitial: () => ScrabbleGameResponse = () => {
    const shuffledTiles = shuffleArray(fullBag, {copy: true});

    return {
        state: {
            finished: false,
            history: [],
            bag: shuffledTiles.slice(14),
            positions: blankBoard,
            currentPlayerIx: 0,
            players: []
        },
        messages: [
            {
                public: true,
                text: "Welcome to your game of scrabble!"
            }
        ]
    };
};

export function parseTurn (state: ScrabbleGameState, message: ScrabbleGameMessage): Turn {
    if (message.command === "play") {
        const [
            where,
            word
        ] = message.text.split(/\s+/u).slice(1);
        let direction: Point; let startPosition: Point;
        const matchHorizontal = where.toUpperCase().match(/^(?<r>[0-9]+)(?<c>[A-P])$/u);
        const matchVertical = where.toUpperCase().match(/^(?<c>[A-P])(?<r>[0-9]+)$/u);

        if (matchHorizontal?.groups) {
            direction = [
                0,
                1
            ];
            startPosition = [
                parseInt(matchHorizontal.groups.r, 10) - 1,
                matchHorizontal.groups.c.charCodeAt(0) - "A".charCodeAt(0)
            ];
        } else if (matchVertical?.groups) {
            direction = [
                1,
                0
            ];
            startPosition = [
                parseInt(matchVertical.groups.r, 10) - 1,
                matchVertical.groups.c.charCodeAt(0) - "A".charCodeAt(0)
            ];
        } else {
            throw `Cannot parse position ${where}`;
        }


        const tilesPlayed: TilePlayed[] = word.split("").map((tile, i): TilePlayed | null => {
            const boardPosition: Point = [
                startPosition[0] + i * direction[0],
                startPosition[1] + i * direction[1]
            ];

            if (boardPosition[0] < 0 ||
                boardPosition[0] >= boardSize ||
                boardPosition[1] < 0 ||
                boardPosition[1] >= boardSize) {
                throw `Play exceeds bounds of board at position ${i + 1}`;

            }

            const isBlank = tile === tile.toLowerCase();
            const tileValue: Tile = isBlank ? "BLANK" : tile.toUpperCase() as Tile;
            const letterValue = (isBlank ? tile.toUpperCase() : tileValue) as TilePlayed["letter"];

            const existingLetter = state.positions[boardPosition[0]][boardPosition[1]].letter;

            if (existingLetter) {
                if (existingLetter !== letterValue) {
                    throw `Mismatch between your word and the board at position ${i + 1},
                    letter ${letterValue} from your play, but ${existingLetter} on the board`;
                }

                return null;
            }

            return {
                tile: tileValue,
                position: boardPosition,
                letter: letterValue
            };
        }).
            filter(<T>(x: T | null): x is T => Boolean(x));

        return {
            turnType: "played-tiles",
            player: message.gamePlayer!,
            played: tilesPlayed
        };
    } else if (message.command === "challenge") {
        return {
            turnType: "issued-challenge",
            player: message.gamePlayer!,
            objectingTo: message.text.split(/\s+/u)[1].toUpperCase()
        };
    } else if (message.command === "trade") {
        const [count] = message.text.split(/\s+/u).slice(1);


        return {
            turnType: "traded-tiles",
            count: count ? parseInt(count, 10) : 7, // TODO check current hand cound and use that as default
            player: message.gamePlayer!
        };
    }

    throw "Could not parse turn";
}


function reduce (state?: ScrabbleGameState, message?: ScrabbleGameMessage): ScrabbleGameResponse {
    try {
        let nextState: ScrabbleGameState;

        switch (message?.command) {
        case undefined:
            return reduceInitial();
        case "join": {
            if (state!.players.length >= 2) {
                throw "Can't add more than two players";
            }
            const players = state!.players.concat([
                {
                    ...message.gamePlayer!,
                    rack: state!.bag.slice(0, 7),
                    score: 0,
                    words: []
                }
            ]);

            const currentPlayerIx = Math.floor(Math.random() * players.length);

            nextState = {
                ...state!,
                currentPlayerIx,
                players,
                bag: state!.bag.slice(7)
            };

            return {
                state: nextState,
                messages: [
                    {
                        public: true,
                        text: renderScrabbleState(nextState),
                        image: boardImage(nextState)
                    }
                ]

            };
        }
        case "first":
            nextState = {
                ...state!,
                currentPlayerIx: state!.players.findIndex((p) => p.id === message.gamePlayer!.id)
            };

            return {
                state: nextState,
                messages: [
                    {
                        public: true,
                        text: renderScrabbleState(nextState),
                        image: boardImage(nextState)
                    }
                ]
            };
        case "play":
        case "challenge":
        case "trade": {
            // Console.log("A move dof play etc", state, message)
            const turn = parseTurn(state!, message);
            const currentPlayer = state!.players[state!.currentPlayerIx];

            if (turn.player.id !== currentPlayer.id) {
                const tried = turn.player.name;
                const correct = currentPlayer.name;

                throw `Player ${tried} tried to take a turn but it's player ${correct}'s turn.`;
            }
            const reduced = reduceTurn(state!, turn);

            nextState = reduced.state;
            const nextMessages = reduced.messages;


            return {
                state: nextState,
                messages: [
                    ...nextMessages,
                    {
                        public: true,
                        text: renderScrabbleState(nextState),
                        image: boardImage(nextState)
                    }
                ]
            };
        }
        default:
            throw "unrecognized command";
        }
    } catch (e) {
        return {
            state: state!,
            messages: [
                {
                    public: true,
                    text: `Error: ${e}`
                }
            ]
        };
    }
}


const addTilesToBoard = (positions: ScrabbleGameState["positions"], tiles: TilePlayed[]) => {
    const nextPositions = deepCopy(positions);

    tiles.forEach((t) => {
        nextPositions[t.position[0]][t.position[1]].tile = t.tile;
        nextPositions[t.position[0]][t.position[1]].letter = t.letter;
    });

    return nextPositions;
};

const adjacentToExistingTiles = (existingPositions: ScrabbleGameState["positions"], played: TilePlayed[]): boolean => {
    const center: Point = [
        Math.floor(boardSize / 2),
        Math.floor(boardSize / 2)
    ];
    const positionsEqualTo = (a: Point) => (b: Point): boolean => a[0] === b[0] && a[1] === b[1];
    const isCenter = positionsEqualTo(center);
    const playedPositions = played.map((x) => x.position);

    if (playedPositions.find(isCenter)) {
        return true;
    }
    const adjacencies = ([
        r,
        c]:
        Point): Point[] => ([
            ...r - 1 >= 0 ? [
                [
                    r - 1,
                    c
                ]
            ] : [],
            ...r + 1 < boardSize ? [
                [
                    r + 1,
                    c
                ]
            ] : [],
            ...c - 1 >= 0 ? [
                [
                    r,
                    c - 1
                ]
            ] : [],
            ...c + 1 < boardSize ? [
                [
                    r,
                    c + 1
                ]
            ] : []
        ]) as Point[];

    if (playedPositions.flatMap(adjacencies).find(([
        r,
        c
    ]) => existingPositions[r][c].tile)) {
        return true;
    }

    return false;
};

export const reduceTurn = (state: ScrabbleGameState, turn: Turn): ScrabbleGameResponse => {
    const nextBoardState = deepCopy(state);
    const messages: GameMessage[] = [];
    const player = nextBoardState.players[nextBoardState.currentPlayerIx];
    const otherPlayerIx = nextBoardState.currentPlayerIx ? 0 : 1;
    const otherPlayer = nextBoardState.players[otherPlayerIx];

    if (turn.turnType === "played-tiles") {
        nextBoardState.positions = addTilesToBoard(nextBoardState.positions, turn.played);
        if (!player) {
            throw `Player ${turn.player.name} not found -- can't take a turn`;
        }
        if (!adjacentToExistingTiles(state.positions, turn.played)) {
            throw "Need to play tiles adjacent to an existing words on the board!";
        }
        for (const t of turn.played) {
            const tileIndexToExcise = player.rack.indexOf(t.tile);

            if (tileIndexToExcise === -1) {
                throw `Can't play a tile you don't have: ${t.tile}from ${player.rack}`;
            }
            player.rack.splice(tileIndexToExcise, 1);
        }
        const {score, words} = scoreTurn(state, turn);

        console.log("Scored", score, words);
        console.log(`scored turn for ${turn.player.id}, ${score}, ${words}`);
        player.score += score;
        player.words.push(words);
        const numToDraw = Math.min(turn.played.length, nextBoardState.bag.length);
        const takenFromBag = nextBoardState.bag.slice(0, numToDraw);

        player!.rack = player!.rack.concat(takenFromBag);
        nextBoardState.bag = nextBoardState.bag.slice(numToDraw);
        nextBoardState.previousTurn = state;
        nextBoardState.currentPlayerIx = otherPlayerIx;

        messages.push({
            public: true,
            text: `
            ${turn.player.name} played ${turn.played.length} tiles for ${score} points.
            ${turn.played.length === 7 ? "Congrats on the 50point bingo!" : ""}
            Words created: ${
    words.map((w) => ` * *${w.word}: ${w.score}`).join("\n")
}`

        });
    } else if (turn.turnType === "traded-tiles") {
        const numToTrade = Math.min(turn.count, player?.rack.length || 0, nextBoardState.bag.length);
        const takenFromBag = nextBoardState.bag.slice(0, numToTrade);
        const takenFromRack = player!.rack.slice(0, numToTrade);

        player!.rack = player!.rack.slice(numToTrade).concat(takenFromBag);
        nextBoardState.bag = nextBoardState.bag.slice(numToTrade).concat(takenFromRack);
        if (player?.words) {
            player.words.push([]);
        }
        player!.words.push([]);
        nextBoardState.currentPlayerIx = otherPlayerIx;
    } else if (turn.turnType === "issued-challenge") {
        const wordOnBoard = otherPlayer?.words.slice(-1)[0].find((w) => w.word === turn.objectingTo);

        if (!wordOnBoard) {
            throw `Can't challenge a word that wasn't played on the last turn: 
            you challenged ${turn.objectingTo} but from ${otherPlayer?.name}'s last turn only
            the following words were played: ${otherPlayer?.words.slice(-1)[0].map((w) => w.word)} `;
        }
        if (allWords[turn.objectingTo]) { // Challenger fails, loses turn
            nextBoardState.currentPlayerIx = otherPlayerIx;
            messages.push({
                public: true,
                text: `
                ${turn.player.name} failed the challenge, losing a turn -- *${turn.objectingTo}* is a valid word.
                ${otherPlayer.name} it's your turn again! 
                `
            });
        } else { // Challenger succeeds, other player loses last turn
            const otherPlayerPreviously = nextBoardState.previousTurn!.players.
                find((p) => p.id !== turn.player.id)!;

            otherPlayer.rack = otherPlayerPreviously?.rack;
            otherPlayer.score = otherPlayerPreviously?.score;
            otherPlayer.words = otherPlayerPreviously?.words.concat([[]]);
            nextBoardState.bag = nextBoardState.previousTurn!.bag;
            nextBoardState.positions = nextBoardState.previousTurn!.positions;
            messages.push({
                public: true,
                text: `
                ${turn.player.name} successfully challenged, -- *${turn.objectingTo}* is *not* a valid word.
                ${otherPlayer.name} you lose your last turn, and ${turn.player.name} is up again. 
                `
            });

        }

    }

    return {
        state: nextBoardState,
        messages
    };
};

const generateCandidateWordLines = (positions: Board, playedTile: TilePlayed): Line[] => {
    const [
        row,
        col
    ] = playedTile.position;

    let left = 0; let right = boardSize - 1; let top = 0; let
        bottom = boardSize - 1;

    for (let c = col - 1; c >= 0; c--) {
        if (!positions?.[row]?.[c].tile) {
            left = c + 1;
            break;
        }
    }
    for (let c = col + 1; c < boardSize; c++) {
        if (!positions?.[row]?.[c].tile) {
            right = c - 1;
            break;
        }
    }
    for (let r = row - 1; r >= 0; r--) {
        if (!positions[r][col].tile) {
            top = r + 1;
            break;
        }
    }
    for (let r = row + 1; r < boardSize; r++) {
        if (!positions?.[r]?.[col].tile) {
            bottom = r - 1;
            break;
        }
    }

    const ret: Line[] = [];

    if (left < right) {
        ret.push([
            [
                row,
                left
            ],
            [
                row,
                right
            ]
        ]);
    }
    if (top < bottom) {
        ret.push([
            [
                top,
                col
            ],
            [
                bottom,
                col
            ]
        ]);
    }

    return ret;
};

const positionsAlong = (l: Line): Point[] => {
    const ret: Point[] = [];

    if (l[0][0] === l[1][0]) { // Same row, different cols
        for (let c = l[0][1]; c <= l[1][1]; c++) {
            ret.push([
                l[0][0],
                c
            ]);
        }
    } else if (l[0][1] === l[1][1]) { // Same col, different rows
        for (let r = l[0][0]; r <= l[1][0]; r++) {
            ret.push([
                r,
                l[0][1]
            ]);
        }
    }

    return ret;
};

interface ScoredWord {
    word: string,
    score: number
}
export const scoreWord = (
    positionsBefore: ScrabbleGameState["positions"],
    positionsAfter: ScrabbleGameState["positions"],
    l: Line
): ScoredWord => {

    const score = positionsAlong(l).reduce((acc, [
        r,
        c
    ]) => {
        const tileBefore = positionsBefore[r][c].tile;
        const tileAfter = positionsAfter[r][c].tile!;
        let letterMultiplier = 1;
        let wordMultiplier = 1;

        if (positionsAfter[r][c].bonus === "double-letter") {
            letterMultiplier = 2;
        } else if (positionsAfter[r][c].bonus === "triple-letter") {
            letterMultiplier = 3;
        } else if (positionsAfter[r][c].bonus === "double-word") {
            wordMultiplier = 2;
        } else if (positionsAfter[r][c].bonus === "triple-word") {
            wordMultiplier = 3;
        }

        const points = tileBefore ? pointValues[tileBefore].score : pointValues[tileAfter].score * letterMultiplier;


        return {
            ...acc,
            wordScore: acc.wordScore + points,
            wordMultiplier: acc.wordMultiplier * (tileBefore ? 1 : wordMultiplier),
            word: acc.word + positionsAfter[r][c].letter?.toUpperCase()
        };
    }, {
        wordMultiplier: 1,
        wordScore: 0,
        word: ""
    });

    return {
        word: score.word,
        score: score.wordMultiplier * score.wordScore
    };
};

interface ScoredTurn {
    score: number,
    words: ScoredWord[]
}
export const scoreTurn = (boardBeforePlay: ScrabbleGameState, turn: Turn): ScoredTurn => {
    if (turn.turnType !== "played-tiles") {
        return {
            score: 0,
            words: []
        };
    }

    const occupiedSquares = turn.
        played.
        filter((t) => Boolean(boardBeforePlay.positions[t.position[0]][t.position[1]].tile));

    if (occupiedSquares.length) {
        throw `Can't play on an occupied square: ${JSON.stringify(occupiedSquares)}`;
    }

    const boardAfterPlay = addTilesToBoard(boardBeforePlay.positions, turn.played);
    const uniqueWordLines = _(turn.played).
        flatMap((t) => generateCandidateWordLines(boardAfterPlay, t)).
        uniqBy(JSON.stringify).
        value();

    const wordScores = uniqueWordLines.map((l) => scoreWord(boardBeforePlay.positions, boardAfterPlay, l));


    return {
        score: wordScores.reduce((acc, w) => acc + w.score, turn.played.length === 7 ? 50 : 0), // Bingo bonus
        words: wordScores
    };
};

function renderScrabbleState (game: ScrabbleGameState): string {
    let ret = "";

    ret += `bag\n${game.bag.join(",")}`;
    ret += `${"board"}\n${game.positions.map((pr) => pr.map((p) => p.tile || "_").join("")).join("\n")}`;
    ret += `Scores\n${JSON.stringify(game.players)}`;
    ret += `Current player index: ${game.currentPlayerIx}`;
    ret += `Current player: ${game.players[game.currentPlayerIx]}`;

    return ret;
}

const boardImage = (board: ScrabbleGameState): string => `${generateMeta().url
}/api/${name}/draw/${
    querystring.escape(board.positions.
        map((row) => row.map((col) => {
            if (col.tile === "BLANK") {
                return col.letter?.toLowerCase();
            } else if (col.tile) {
                return col.tile;
            }

            return " ";

        }).join("")).join(","))}`;


const router = makeRouter<ScrabbleGameState, ScrabbleGameMessage>(commands, reduce);

interface DrawableBoard {
    board: string[][]
}
router.get(
    "/draw/:spec",
    async (req, res) => {
        // Const spec = JSON.parse(base64url.decode(req.params.spec)) as unknown as DrawableBoard;
        const spec = {
            board: querystring.unescape(req.params.spec).split(",").
                map((r) => r.split(""))
        } as unknown as DrawableBoard;
        const boardWidth = 1000;
        const boardHeight = 1000;
        const topLeft = 61;
        const bottomRight = 938;
        const boardSquareSize = (bottomRight - topLeft) / 15;
        const tileMargin = 2;

        const BOARD_IMAGE_NAME = "Blank_Scrabble_board_with_coordinates.png";
        const boardImageContent = await loadImage(`${__dirname}/${BOARD_IMAGE_NAME}`);
        const canvas = createCanvas(boardWidth, boardHeight);

        const ctx = canvas.getContext("2d");

        ctx.fillStyle = "white";
        ctx.fillRect(
            0,
            0,
            canvas.width,
            canvas.height
        );
        ctx.drawImage(boardImageContent, 0, 22, 1000, 956);

        spec.board.forEach((rvals, row) => {
            rvals.forEach((tileVal, col) => {

                const isGap = tileVal.trim() === "";

                if (isGap) {
                    return;
                }

                const isBlank = tileVal !== tileVal.toUpperCase();
                const tilePoints = isBlank ? pointValues.BLANK : pointValues[tileVal as Tile];

                ctx.fillStyle = "white";
                ctx.fillRect(
                    topLeft + boardSquareSize * col,
                    topLeft + boardSquareSize * row,
                    boardSquareSize - 1,
                    boardSquareSize - 1
                );
                ctx.fillStyle = "tan";
                ctx.fillRect(
                    topLeft + boardSquareSize * col + tileMargin,
                    topLeft + boardSquareSize * row + tileMargin,
                    boardSquareSize - 2 * tileMargin,
                    boardSquareSize - 2 * tileMargin
                );
                ctx.textAlign = "center";
                ctx.font = "bold 40px Droid Sans";
                ctx.fillStyle = isBlank ? "slateblue" : "black";
                ctx.fillText(
                    tileVal.toUpperCase(),
                    topLeft + (0.4 + col) * boardSquareSize,
                    topLeft + (0.5 + row) * boardSquareSize + boardSquareSize / 4
                );
                ctx.fillStyle = "black";
                ctx.font = "bold 18px Droid Sans";
                ctx.textAlign = "right";
                ctx.fillText(
                    `${tilePoints.score}`,
                    topLeft + (0.94 + col) * boardSquareSize,
                    topLeft + (0.6 + row) * boardSquareSize + boardSquareSize / 4
                );


            });
        });

        canvas.createPNGStream().pipe(res);
    }
);


export default {
    name,
    router
};
