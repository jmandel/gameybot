import base64url from "base64-url";
import {createCanvas} from "canvas";
import rfdc from "rfdc";
import shuffleArray from "shuffle-array";
import {generateMeta} from "./config";
import {GameMessage, GameState, GamePlayer} from "./GameInterfaces";
import words from "./words.json";
import {makeRouter} from "./game-routes";

const name = "codenames";

const deepCopy = rfdc();

type CodenamesGameCommand = "begin" | "spymaster" | "clue" | "done" | "guess"
type CodenamesCardColor = "red" | "blue" | "black" | "tan"

interface CodenamesGameMessage extends GameMessage {
    command: CodenamesGameCommand
}

interface CodenamesGameState extends GameState {
    history: CodenamesGameMessage[],
    spymasterRed?: GamePlayer,
    spymasterBlue?: GamePlayer,
    cluingTeam: CodenamesCardColor,
    clue?: {
        word: string,
        count: number
    },
    guesses: string[],
    board: {
        word: string,
        color: CodenamesCardColor,
        revealed: boolean
    }[]
}

type DrawableBoard = (CodenamesGameState["board"][number] & { hideText?: boolean })[]

const commands: {
    command: CodenamesGameCommand,
    description: string
}[] = [
    {
        command: "spymaster",
        description: "claim the role of spymaster on the red or blue team. Example: `%spymaster red`"
    },
    {
        command: "clue",
        description: "provide a clue for your team. Example: `%clue watershed 3`"
    },
    {
        command: "done",
        description: "indicate that you're done guessing for this turn -- if you're stopping early. Example: `%done`"
    },
    {
        command: "guess",
        description: "submit a guess (or several) for your team. Example: `%guess crane bark hand`"
    }
];

const boardImage = (board: DrawableBoard): string => `${generateMeta().url
}/api/codenames/draw/${
    base64url.encode(JSON.stringify(board))}`;

const spymasterBoardImage = (board: CodenamesGameState["board"]) => boardImage(board.map((w) => ({
    ...w,
    revealed: true,
    hideText: w.revealed
})));

const otherTeam = (t: CodenamesCardColor): CodenamesCardColor => t === "red" ? "blue" : "red";

const initialState = () => {
    // Start a new game

    const firstPlayer = Math.random() < 0.5 ? "red" : "blue";
    const shuffledWords = shuffleArray([...words]).slice(
        0,
        25
    );
    const shuffledColors: CodenamesCardColor[] = shuffleArray([
        firstPlayer,
        "black",
        ...Array(8).fill("red"),
        ...Array(8).fill("blue"),
        ...Array(7).fill("tan")
    ]);
    const nextState: CodenamesGameState = {
        finished: false,
        cluingTeam: firstPlayer,
        guesses: [],
        board: Array(25).fill(0).
            map((_, i) => ({
                word: shuffledWords[i],
                color: shuffledColors[i],
                revealed: false
            })),
        history: []
    };

    return {
        state: nextState,
        messages: [
            {
                public: true,
                text: `Welcome to Codenames! ${firstPlayer} team goes first.\n` +
                    "Type `%spymaster red` or `%spymaster blue` to claim the spymaster role.",
                image: boardImage(nextState.board)
            }
        ]
    };

};

function reduce (state?: CodenamesGameState, message?: CodenamesGameMessage): {
    state: CodenamesGameState,
    messages: GameMessage[]
} {

    if (!state) {
        return initialState();
    }

    if (message?.command === "spymaster") {
        return reduceSpymaster(state, message);

    }

    if (message?.command === "clue") {
        return reduceClue(state, message);
    }


    if (message?.command === "guess" || message?.command === "done") {
        return reduceGuess(state, message);
    }

    return {
        state,
        messages: [message] as GameMessage[]
    };

}

function reduceSpymaster (state: CodenamesGameState, message: CodenamesGameMessage) {
    const forTeam = message.text.trim().split(/\s+/u)[1] as CodenamesCardColor;
    const spymasterProperty = forTeam === "red" ? "spymasterRed" : "spymasterBlue";
    const nextState = {
        ...state,
        history: [
            ...state.history,
            message
        ],
        [spymasterProperty]: message.gamePlayer
    };

    return {
        state: nextState,
        messages: [
            {
                public: true,
                text: `${message.gamePlayer!.name} is the spymaster for ${forTeam}. ${
                    nextState.spymasterBlue && nextState.spymasterRed
                        ? `${nextState.cluingTeam}, your turn to give a clue when ready.
                    (Ex: \`%clue foosball 3\`.)`
                        : ""
                }`
            },
            {
                public: false,
                gamePlayer: message.gamePlayer!,
                text: "As the spymaster, you get access to the hidden board state!",
                image: spymasterBoardImage(state.board)
            }
        ]
    };
}


function reduceClue (state: CodenamesGameState, message: CodenamesGameMessage) {

    if (!state.spymasterBlue || !state.spymasterRed) {
        return {
            state,
            messages: [
                {
                    public: true,
                    text: "Please tell me who the `%spymaster` is before giving a clue!"
                }
            ]
        };
    }

    const [
        word,
        count
    ] = message?.text.trim().split(/\s+/u).slice(1);


    const nextState = {
        ...state,
        clue: {
            word,
            count: parseInt(count, 10)
        }
    };


    return {
        state: nextState,
        messages: [
            {
                public: true,
                text: `Okay *${nextState.cluingTeam}*, you can make up to *${
                    nextState.clue.count + 1}* guesses,
            of which ${nextState.clue.count} should be about the word *${nextState.clue.word}*.`
            }
        ]
    };
}

function reduceGuess (state: CodenamesGameState, message: CodenamesGameMessage) {

    const board = deepCopy(state.board);
    const countedGuessesThisRound = deepCopy(state.guesses);
    const results = [];

    let endOfTurn = message?.command === "done";
    let guessedBlack = false;

    const guesses = message.text.toUpperCase().trim().
        split(/\s+/u).
        slice(1);

    const processGuesses = (acc: { unmatched: string[], cards: CodenamesGameState["board"] }, nextGuess: string) => {
        const unmatched = [
            ...acc.unmatched,
            nextGuess
        ];
        const toMatch = unmatched.join(" ");
        const card = board.find((c) => c.word === toMatch);

        if (card) {
            return {
                ...acc,
                unmatched: [],
                cards: [
                    ...acc.cards,
                    card
                ]
            };
        }

        return {
            ...acc,
            unmatched
        };
    };

    const validGuesses = guesses.reduce(processGuesses, {
        unmatched: [],
        cards: []
    });

    console.log("Reviewed Guesses", guesses, validGuesses);


    if (validGuesses.unmatched.length > 0) {
        return {
            state,
            messages: [
                {
                    public: true,
                    text: `That guess included an unrecognized word: ${validGuesses.unmatched[0]}. Please try again.`
                }
            ]

        };
    }

    for (const guessedCard of validGuesses.cards) {
        guessedCard.revealed = true;
        countedGuessesThisRound.push(guessedCard.word);
        results.push(`"*${guessedCard.word}*" was *${guessedCard.color}*.`);

        const guessedWrongColor = guessedCard.color !== state.cluingTeam;
        const guessedMaxWords = countedGuessesThisRound.length >= (state.clue?.count || 0) + 1;

        if (guessedWrongColor || guessedMaxWords) {
            endOfTurn = true;
            if (guessedCard.color === "black") {
                guessedBlack = true;
            }
            break;
        }
    }

    let endOfGame = false;
    let winner: CodenamesCardColor | null = null;
    const remaining = board.filter((w) => !w.revealed && w.color === state.cluingTeam).length;

    if (remaining === 0) {
        endOfGame = true;
        winner = state.cluingTeam;
    } else if (guessedBlack) {
        endOfGame = true;
        winner = otherTeam(state.cluingTeam);
    }

    const publicMessage = `${results.join("\n")}`;

    const messagesToSpymasters = (forState: CodenamesGameState) => [
        forState.spymasterBlue,
        forState.spymasterRed
    ].map((spymaster) => ({
        public: false,
        gamePlayer: spymaster,
        text: "Your board",
        image: spymasterBoardImage(board)
    }));

    if (endOfGame) {
        return {
            state: {
                ...state,
                finished: true,
                board,
                history: [
                    ...state.history,
                    message
                ],
                guesses: countedGuessesThisRound
            },
            messages: [
                {
                    public: true,
                    text: `${publicMessage}\nAnd that's the game! Congratulations ${winner} team!`,
                    image: boardImage(board)
                },
                {
                    public: true,
                    text: "... and for the record, here's the full board:",
                    image: spymasterBoardImage(board.map((w) => ({...w,
                        revealed: false})))
                }
            ]
        };
    }

    if (!endOfTurn) {
        const nextState = {
            ...state,
            board,
            history: [
                ...state.history,
                message
            ],
            guesses: countedGuessesThisRound
        };


        return {
            state: nextState,
            messages: [
                {
                    public: true,
                    text: `${
                        publicMessage}\nKeep guessing or say \`%done\` if you want to hand it back to ${
                        otherTeam(state.cluingTeam)}.`,
                    image: boardImage(board)
                }
            ]
        };
    }

    const nextState: CodenamesGameState = {
        ...state,
        guesses: [],
        clue: undefined,
        history: [
            ...state.history,
            message
        ],
        board,
        cluingTeam: otherTeam(state.cluingTeam)
    };

    return {
        state: nextState,
        messages: [
            {
                public: true,
                text: `${publicMessage}\nNext up, ${nextState.cluingTeam}!`,
                image: boardImage(board)
            },
            ...messagesToSpymasters(nextState)
        ]
    };
}

const router = makeRouter<CodenamesGameState, CodenamesGameMessage>(commands, reduce);

router.get(
    "/draw/:spec",
    (req, res) => {
        const spec = JSON.parse(base64url.decode(req.params.spec)) as unknown as DrawableBoard;

        const colWidth = 150;
        const rowHeight = 80;
        const pad = 5;
        const canvas = createCanvas(
            colWidth * 5 + pad * 6,
            rowHeight * 5 + pad * 6
        );
        const ctx = canvas.getContext("2d");

        ctx.fillStyle = "white";
        ctx.fillRect(
            0,
            0,
            canvas.width,
            canvas.height
        );
        ctx.textAlign = "center";
        ctx.font = "bold 20px Droid Sans";
        spec.forEach((w, i) => {
            const row = Math.floor(i / 5);
            const col = i % 5;

            if (w.revealed) {
                ctx.fillStyle = w.color;
            } else {
                ctx.fillStyle = "white";
            }
            ctx.fillRect(
                pad + (colWidth + pad) * col,
                pad + (rowHeight + pad) * row,
                colWidth,
                rowHeight
            );

            if (w.revealed) {
                ctx.fillStyle = "white";
            } else {
                ctx.fillStyle = "black";
            }

            if (!w.hideText) {
                ctx.fillText(
                    w.word,
                    (col + 1) * pad + colWidth * (col + 0.5),
                    (row + 1) * pad + rowHeight * (row + 0.6)
                );
            }
        });

        canvas.createPNGStream().pipe(res);
    }
);

export default {
    name,
    router
};
