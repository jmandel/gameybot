#![allow(dead_code)]

use std::collections::HashMap;
use std::fs::File;
use std::io::prelude::*;
use std::io::BufReader;
use std::iter;
use std::fmt::Debug;

type Point = (usize, usize);

#[derive(Debug, Clone, Copy, Hash, PartialEq, Eq, PartialOrd, Ord)]
enum Letter {
    A,
    B,
    C,
    D,
    E,
    F,
    G,
    H,
    I,
    J,
    K,
    L,
    M,
    N,
    O,
    P,
    Q,
    R,
    S,
    T,
    U,
    V,
    W,
    X,
    Y,
    Z,
}

#[derive(Debug, Clone, Copy, Eq, PartialEq, Hash)]
enum Tile {
    Blank(Option<Letter>),
    Marked(Letter),
}

#[derive(Debug, Clone, Copy)]
struct Bonus {
    letter_x: usize,
    word_x: usize,
}

#[derive(Debug, Clone)]
struct BoardPosition {
    tile: Option<Tile>,
    bonus: Bonus,
    allowed_letters_cache: Option<HashMap<Letter,bool>>
}

#[derive(Debug, Clone)]
struct Board(HashMap<Point, BoardPosition>);

impl Board {
    fn transpose(&self) -> Board {
        let mut ret = self.clone();
        for &(r, c) in self.0.keys() {
            ret.0.insert((c, r), self.0.get(&(r, c)).unwrap().clone());
        }
        ret
    }

    fn is_anchor(&self, (r, c): Point) -> bool {
        let (r, c) = (r as isize, c as isize);

        let notself = self.0.get(&(r as usize,c as usize)).unwrap().tile.is_none();
        
        let notadj = vec![
            (r - 1, c), (r + 1, c), (r, c - 1), (r, c + 1)]
            .iter()
            .copied()
            .filter(|&(r, c)| {
                r >= 0 && r < BOARD_SIZE as isize && c >= 0 && c < BOARD_SIZE as isize
            })
            .any(|pos| {
                let subpos=self.0
                    .get(&(pos.0 as usize, pos.1 as usize))
                    .unwrap().tile.is_some();
                // println!("Sbpos, {:?}, {:?}", pos, self.0
                    // .get(&(pos.0 as usize, pos.1 as usize)));
                subpos
            });

        // println!("is_a {:?},{:?}, : {:?} {:}", r, c, notself, notadj);
        return notself && notadj;
    }

    fn letters_allowed_at(&mut self, (r, c): Point, d: &TrieNode) -> Vec<Letter> {

        if r>= BOARD_SIZE || c >= BOARD_SIZE {
            return vec![];
        }

        let cached = self.0.get(&(r,c)).and_then(|bp| bp.allowed_letters_cache.as_ref());
        match cached {
            Some(c) => {
                return c.keys().copied().collect();
            },
            None => {}
        }


        let along = |range: &[usize]| -> Vec<Letter> {
            range
                .iter()
                .map(|r| self.0.get(&(*r, c)).unwrap())
                .take_while(|&bp| bp.tile.is_some())
                .map(|bp| match bp.tile.unwrap() {
                    Tile::Marked(letter) => letter,
                    Tile::Blank(Some(letter)) => letter,
                    Tile::Blank(None) => panic!("Tile exists but is not marked or bound"),
                })
                .collect()
        };

        let before_range: Vec<usize> = (0..r).rev().collect();
        let after_range: Vec<usize> = (r + 1..BOARD_SIZE).collect();
        let before = along(&before_range);
        let after = along(&after_range);

        let ret = d.valid_interstitials(&before, &after);
        
        let mut to_cache = HashMap::new();
        for v in &ret  {
            to_cache.insert(*v, true);
        }
        self.0.get_mut(&(r,c)).unwrap().allowed_letters_cache = Some(to_cache);
        ret
    }
}

fn score_word(board: &Board, start: &Point, end: &Point) -> usize {
    if start.0 != end.0 && start.1 != end.1 {
        panic!("Words must be co-linear!")
    }

    let mut letter_points = 0;
    let mut word_x = 1;

    for r in start.0..=end.0 {
        for c in start.1..=end.1 {
            let board_position = board.0.get(&(r, c)).unwrap();
            let (letter_bonus, word_bonus) = if board_position.tile.is_some() {
                (1, 1)
            } else {
                (board_position.bonus.letter_x, board_position.bonus.word_x)
            };

            letter_points += 1 * letter_bonus;
            word_x *= word_bonus;
        }
    }
    letter_points * word_x
}

impl From<Tile> for Letter {
    fn from(t: Tile) -> Letter {
        match t {
            Tile::Marked(l) => l,
            Tile::Blank(Some(l)) => l,
            _ => panic!("Can't recurse with an unbound blank tile"),
        }
    }
}

impl From<char> for Letter {
    fn from(c: char) -> Letter {
        match c.to_ascii_lowercase() {
            'a' => Letter::A,
            'b' => Letter::B,
            'c' => Letter::C,
            'd' => Letter::D,
            'e' => Letter::E,
            'f' => Letter::F,
            'g' => Letter::G,
            'h' => Letter::H,
            'i' => Letter::I,
            'j' => Letter::J,
            'k' => Letter::K,
            'l' => Letter::L,
            'm' => Letter::M,
            'n' => Letter::N,
            'o' => Letter::O,
            'p' => Letter::P,
            'q' => Letter::Q,
            'r' => Letter::R,
            's' => Letter::S,
            't' => Letter::T,
            'u' => Letter::U,
            'v' => Letter::V,
            'w' => Letter::W,
            'x' => Letter::X,
            'y' => Letter::Y,
            'z' => Letter::Z,
            _ => panic!("Unrecognized letter"),
        }
    }
}

#[derive(Debug)]
struct Trie<T> {
    suffixes: HashMap<T, Trie<T>>,
    is_word: bool,
}

struct RightPartIteratorStackFrame<'a> {
    arg_partial_word: Vec<Tile>,
    arg_square: Point,
    arg_tile_from_hand: bool,
    state_edges: Box<dyn Iterator<Item = &'a Letter> + 'a>,
    finished_emit: bool,
    finished_execution: bool,
}

struct RightPartIterator<'a> {
    rack: Rack,
    board: &'a mut Board,
    dictionary: &'a TrieNode,
    stack: Vec<RightPartIteratorStackFrame<'a>>,
}
impl<'a> Iterator for RightPartIterator<'a> {
    type Item = Vec<Letter>;
    fn next(&mut self) -> Option<Self::Item> {
        let mut state = self.stack.pop()?;
        let mut trie = self
            .dictionary
            .advance(state.arg_partial_word.iter().map(|&t| Letter::from(t)));
        // println!("ITer {:?} {:?} @ {:?}, allows [{:?}] of rack {:?}", state.arg_partial_word, state.finished_execution, state.arg_square, trie.suffixes.keys(), self.rack);

        if state.arg_square.1 > BOARD_SIZE {
            state.finished_execution = true;
            self.stack.push(state);
            return self.next();
        }
        if state.finished_execution {
            if state.arg_tile_from_hand {
                let tile_to_replace = state.arg_partial_word.pop().unwrap();
                self.rack.replace_tile(tile_to_replace);
            }
            // println!("Cleaned up leaving stack depth {:?}", self.stack.len());
            return self.next();
        }

        // println!("Open board at {:?}", state.arg_square);
        let board_square = self.board.0.get(&state.arg_square).and_then(|pos|pos.tile);
        // println!("At board sre {:?} word {:?}", state.arg_square, state.arg_partial_word);
        if board_square.is_none() {
            if !state.finished_emit && trie.is_word {
                let ret = state
                    .arg_partial_word
                    .iter()
                    .map(|&t| Letter::from(t))
                    .collect();
                state.finished_emit = true;
                self.stack.push(state);
                return Some(ret);
            }
            let e = state.state_edges.next();
            if e.is_none() {
                // println!("No edges from herei {:?}", state.arg_partial_word);
                state.finished_execution = true;
                self.stack.push(state);
                return self.next();
            }
            let e = e.unwrap();
            // println!("And try {:?} -> edge {:?}", state.arg_partial_word, e);
            if !self
                .board
                .letters_allowed_at(state.arg_square, self.dictionary)
                .contains(e)
            {
                self.stack.push(state);
                return self.next();
            }
            match self.rack.consume_tile(*e) {
                None => {
                    self.stack.push(state);
                    return self.next();
                }
                Some(t) => {
                    if trie.suffixes.get(&Letter::from(t)).is_none(){
                        state.finished_execution = true;
                        self.stack.push(state);
                        return self.next();
                    }
                    trie = trie.advance(iter::once(Letter::from(t)));
                    let next_square = (state.arg_square.0, state.arg_square.1 + 1);
                    let mut next_partial_word = state.arg_partial_word.clone();
                    next_partial_word.push(t);
                    self.stack.push(state);
                    self.stack.push(RightPartIteratorStackFrame {
                        arg_tile_from_hand: true,
                        arg_partial_word: next_partial_word,
                        arg_square: next_square,
                        state_edges: Box::new(trie.suffixes.keys()),
                        finished_emit: false,
                        finished_execution: false,
                    });
                    return self.next();
                }
            }
        } else {
            let existing_tile = board_square.unwrap();
            match trie.suffixes.get(&Letter::from(existing_tile)) {
                None => {
                    state.finished_execution = true;
                    self.stack.push(state);
                    return self.next();
                },
                Some(_) => {
                    let next_square = (state.arg_square.0, state.arg_square.1 + 1);
                    trie = trie.advance(iter::once(Letter::from(existing_tile)));
                    let mut next_partial_word = state.arg_partial_word.clone();
                    next_partial_word.push(existing_tile);
                    state.finished_execution = true;
                    self.stack.push(state);
                    self.stack.push(RightPartIteratorStackFrame {
                        arg_tile_from_hand: false,
                        arg_partial_word: next_partial_word,
                        arg_square: next_square,
                        state_edges: Box::new(trie.suffixes.keys()),
                        finished_execution: false,
                        finished_emit: false,
                    });
                    return self.next();
                }
            }
        }
    }
}

struct LeftPartIteratorStackFrame<'a> {
    arg_partial_word: Vec<Tile>,
    arg_limit: usize,
    state_edges: Box<dyn Iterator<Item = &'a Letter> + 'a>,
}

// TODO: benchmark performance using mutation over Rack, vs return-new-from-letter
struct LeftPartIterator<'a> {
    rack: Rack,
    dictionary: &'a TrieNode,
    max_length: usize,
    stack: Vec<LeftPartIteratorStackFrame<'a>>,
    first_run: bool,
}

impl<'a> Iterator for LeftPartIterator<'a> {
    type Item = (Vec<Letter>, Rack);
    fn next(&mut self) -> Option<(Vec<Letter>, Rack)> {
        if self.first_run {
            self.first_run = false;
            self.stack.push(LeftPartIteratorStackFrame {
                arg_partial_word: vec![],
                arg_limit: self.max_length,
                state_edges: Box::new(self.dictionary.suffixes.keys()),
            });
            return Some((vec![], self.rack.clone()));
        }

        let mut stack_frame_current = self.stack.pop()?;
        let next_letter = stack_frame_current.state_edges.next();

        if stack_frame_current.arg_limit == 0 || next_letter.is_none() {
            let tile_to_replace = stack_frame_current.arg_partial_word.pop()?;
            self.rack.replace_tile(tile_to_replace);
            return self.next();
        }

        match self.rack.consume_tile(*next_letter.unwrap()) {
            None => {
                self.stack.push(stack_frame_current);
                return self.next();
            }
            Some(t) => {
                let partial_word_letters = stack_frame_current
                    .arg_partial_word
                    .iter()
                    .chain(iter::once(&t))
                    .map(|&t| Letter::from(t));

                let trie_node = self.dictionary.advance(partial_word_letters);

                let stack_frame_next = LeftPartIteratorStackFrame {
                    arg_partial_word: stack_frame_current
                        .arg_partial_word
                        .iter()
                        .chain([t].iter())
                        .copied()
                        .collect(),
                    state_edges: Box::new(trie_node.suffixes.keys()),
                    arg_limit: stack_frame_current.arg_limit - 1,
                };

                let ret = Some((
                    stack_frame_next
                        .arg_partial_word
                        .iter()
                        .map(|t| Letter::from(*t))
                        .collect(),
                    self.rack.clone(),
                ));

                self.stack.push(stack_frame_current);
                self.stack.push(stack_frame_next);
                return ret;
            }
        }
    }
}

#[derive(Clone, Debug)]
struct Rack {
    tiles: HashMap<Tile, usize>,
}

impl Rack {
    fn replace_tile(&mut self, t: Tile) {
        let replacement = match t {
            Tile::Blank(Some(_)) => Tile::Blank(None),
            _ => t,
        };

        *self.tiles.entry(replacement).or_default() += 1;
    }

    fn consume_tile(&mut self, l: Letter) -> Option<Tile> {
        let marked = Tile::Marked(l);
        let blank = Tile::Blank(None);

        let exists_marked = self.tiles.entry(marked).or_default();
        if *exists_marked > 0 {
            *exists_marked -= 1;
            return Some(marked);
        }
        let exists_blank = self.tiles.entry(blank).or_default();
        if *exists_blank > 0 {
            *exists_blank -= 1;
            return Some(Tile::Blank(Some(l)));
        }

        None
    }

    fn find_right_parts<'a>(
        &'a self,
        partial_word: Vec<Letter>,
        position: Point,
        board: &'a mut Board,
        dictionary: &'a TrieNode,
    ) -> RightPartIterator<'a> {
        RightPartIterator {
            rack: self.clone(),
            board: board,
            dictionary: dictionary,
            stack: vec![RightPartIteratorStackFrame {
                arg_partial_word: partial_word.iter().map(|&l| Tile::Marked(l)).collect(),
                arg_square: position,
                arg_tile_from_hand: false,
                state_edges: Box::new(
                    dictionary
                        .advance(partial_word.iter().copied())
                        .suffixes
                        .keys(),
                ),
                finished_execution: false,
                finished_emit: false,
            }],
        }
    }

    fn find_left_parts<'a>(&'a self, dictionary: &'a TrieNode, max_length: usize) -> LeftPartIterator<'a> {
        LeftPartIterator {
            rack: self.clone(),
            dictionary: dictionary,
            stack: vec![],
            first_run: true,
            max_length
        }
    }

    fn all_plays_for_row(&mut self, board: &mut Board, r: usize, dictionary: &TrieNode) -> Vec<(Point, Vec<Letter>)> {
        let positions_in_row = (0..BOARD_SIZE).map(|c| (r, c));

        let anchors: Vec<Point> = positions_in_row
            .clone()
            .filter(|&p| board.is_anchor(p))
            .collect();

        let crosscheck_needs: Vec<Vec<Letter>> = positions_in_row
            .map(|p| board.letters_allowed_at(p, dictionary))
            .collect();

        let mut ret = vec![];
        for a in anchors {
            // println!("Anchor eval {:?}", a);
            for (left_letters, rack) in self.find_left_parts(dictionary, a.1) {
                // println!("Evaluate lhs {:?}", left_letters);
                for right_letters in rack.find_right_parts(left_letters.clone(), a, board, dictionary) {
                    // println!("Evaluate RHS {:?} -> {:?}", left_letters, right_letters);
                    ret.push(((a.0, a.1-left_letters.len()), right_letters));
                }
            }
        }

        ret
    }

    fn best_play(&mut self, board: &mut Board, d: &TrieNode) -> (Point, bool, Vec<Letter>) {
        let transposed = board.transpose();
        let rows = 0..BOARD_SIZE;

        let row_plays: Vec<Vec<(Point, Vec<Letter>)>> = rows
            .clone()
            .map(|r| self.all_plays_for_row(board, r, d))
            .collect();
// 
        // let col_plays: Vec<Vec<(Point, Vec<Letter>)>> = rows
        //     .clone()
        //     .map(|r| self.all_plays_for_row(&transposed, r, d))
        //     .collect();
        
        for ps in row_plays{
            for p in ps {
                //println!("Possible play {:?}", p)

            }
        } 
        // for ps in col_plays{
        //     for p in ps {
        //         println!("Possible play TRANSPOSE {:?}", p)
                
        //     }
        // }


        ((0,0), true, vec![])
    }
}

static ALPHABET: [Letter; 26] = [
    Letter::A,
    Letter::B,
    Letter::C,
    Letter::D,
    Letter::E,
    Letter::F,
    Letter::G,
    Letter::H,
    Letter::I,
    Letter::J,
    Letter::K,
    Letter::L,
    Letter::M,
    Letter::N,
    Letter::O,
    Letter::P,
    Letter::Q,
    Letter::R,
    Letter::S,
    Letter::T,
    Letter::U,
    Letter::V,
    Letter::W,
    Letter::X,
    Letter::Y,
    Letter::Z,
];

type TrieNode = Trie<Letter>;
fn build_dictionry(words: &Vec<String>) -> TrieNode {
    let mut dictionary: Trie<Letter> = Trie::default();
    for w in words {
        let mut t = &mut dictionary;
        for c in w.chars() {
            let letter = Letter::from(c);
            t = t.suffixes.entry(letter).or_default()
        }
        t.is_word = true
    }
    dictionary
}

impl Default for Trie<Letter> {
    fn default() -> Trie<Letter> {
        Trie {
            is_word: false,
            suffixes: HashMap::new(),
        }
    }
}

impl<T: Eq + std::hash::Hash + Debug> Trie<T> {
    fn advance(&self, values: impl Iterator<Item = T>) -> &Trie<T> {
        let mut t = self;
        for v in values {
            // println!("Advance {:?}", v);
            t = t.suffixes.get(&v).unwrap();
        }
        t
    }
}

fn dfs(path: &str, t: &Trie<Letter>) -> () {
    if t.is_word {
        println!("{}", path);
    }
    for (l, tsuffix) in t.suffixes.iter() {
        dfs(&format!("{}{:?}", path, l), tsuffix)
    }
}

impl TrieNode {
    fn is_valid_word(&self, letters: &[Letter]) -> bool {
        let mut d = self;
        for l in letters {
            match d.suffixes.get(l) {
                Some(dnext) => d = dnext,
                None => return false,
            }
        }
        return d.is_word;
    }

    fn valid_interstitials(&self, prefix: &[Letter], suffix: &[Letter]) -> Vec<Letter> {
        if prefix.len() + suffix.len() == 0 {
            Vec::from(ALPHABET)
        } else {
            ALPHABET
                .iter()
                .filter(|&&l| {
                    self.is_valid_word(
                        &vec![prefix, &[l], suffix]
                            .into_iter()
                            .flatten()
                            .copied()
                            .collect::<Vec<_>>(),
                    )
                })
                .copied()
                .collect()
        }
    }
}

static BOARD_SIZE: usize = 15;
static BONUS_LAYOUT: &str = "
W  l   W   l  W|
 w   L   L   w |
  w   l l   w  |
l  w   l   w  l|
    w     w    |
 L   L   L   L |
  l   l l   l  |
W  l   w   l  W|
  l   l l   l  |
 L   L   L   L |
    w     w    |
l  w   l   w  l|
  w   l l   w  |
 w   L   L   w |
W  l   W   l  W|
";

fn main() {
    let mut board: Board = Board(HashMap::new());
    let bonus_board: Vec<&str> = BONUS_LAYOUT
        .trim()
        .split("|")
        .map(|s| s.trim_matches('\n'))
        .take_while(|s| s.len() > 0)
        .collect();

    for r in 0..BOARD_SIZE {
        for c in 0..BOARD_SIZE {
            board.0.insert(
                (r, c),
                BoardPosition {
                    tile: None,
                    allowed_letters_cache: None,
                    bonus: match bonus_board[r as usize]
                        .get(c as usize..=c as usize)
                        .unwrap()
                    {
                        "W" => Bonus {
                            letter_x: 1,
                            word_x: 3,
                        },
                        "w" => Bonus {
                            letter_x: 1,
                            word_x: 2,
                        },
                        "L" => Bonus {
                            letter_x: 3,
                            word_x: 1,
                        },
                        "l" => Bonus {
                            letter_x: 2,
                            word_x: 1,
                        },
                        " " => Bonus {
                            letter_x: 1,
                            word_x: 1,
                        },
                        _ => panic!(
                            "unexpected character in bonus board layout {}, {}, {:?}",
                            r,
                            c,
                            bonus_board[r as usize].get(c as usize..=c as usize)
                        ),
                    },
                },
            );
        }
    }

    let mut words = vec![];
    let mut file = File::open("../src/Collins-Scrabble-Words-2019.txt").unwrap();
    // let file = File::open("tiny-dictionary.txt").unwrap();
    let reader = BufReader::new(file);
    for line in reader.lines() {
        words.push(line.unwrap().to_string());
    }

    println!("Build dict for: {:?}", words.len());
    let dictionary = build_dictionry(&words);
    println!("Done");

    //dfs("", &dictionary);
    println!(
        "VAli? {}",
        dictionary.is_valid_word(&vec![Letter::C, Letter::A, Letter::B, Letter::X])
    );

    board.0.get_mut(&(7, 8)).unwrap().tile = Some(Tile::Marked(Letter::C));
    board.0.get_mut(&(7, 9)).unwrap().tile = Some(Tile::Marked(Letter::R));
    board.0.get_mut(&(7, 10)).unwrap().tile = Some(Tile::Marked(Letter::A));
    board.0.get_mut(&(7, 11)).unwrap().tile = Some(Tile::Marked(Letter::B));
    board.0.get_mut(&(7, 12)).unwrap().tile = Some(Tile::Marked(Letter::B));
    board.0.get_mut(&(7, 13)).unwrap().tile = Some(Tile::Marked(Letter::L));
    board.0.get_mut(&(7, 14)).unwrap().tile = Some(Tile::Marked(Letter::E));

    println!(
        "completions {:?}",
        dictionary.valid_interstitials(&vec![], &vec![Letter::B])
    );
    println!("tarnsposed {:?}", board.transpose().0.get(&(5, 0)));

    let mut rack: Rack = Rack {
        tiles: HashMap::new(),
    };
    rack.replace_tile(Tile::Marked(Letter::R));
    rack.replace_tile(Tile::Marked(Letter::A));
    rack.replace_tile(Tile::Marked(Letter::C));
    rack.replace_tile(Tile::Marked(Letter::T));
    // rack.replace_tile(Tile::Marked(Letter::O));
    // rack.replace_tile(Tile::Marked(Letter::Y));
    rack.replace_tile(Tile::Marked(Letter::P));
    rack.replace_tile(Tile::Blank(None));
    rack.replace_tile(Tile::Blank(None));

    println!("Valid {:?}", board.letters_allowed_at((6,10), &dictionary));
    
    let guard = pprof::ProfilerGuard::new(100).unwrap();
    for _ in 0..20 {
    rack.best_play(&mut board,   &dictionary);
    }
    if let Ok(report) = guard.report().build() {
    let file = File::create("flamegraph.svg").unwrap();
    report.flamegraph(file).unwrap();
};
    // for p in rack.find_right_parts(vec![Letter::C], (1, 4), &board, &dictionary) {
    //     println!("Foudn RP {:?}", p);
    // }
}
